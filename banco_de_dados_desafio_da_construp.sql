-- -------------------------------------------------------------------------
-- banco_de_dados_desafio_da_construp

DROP SCHEMA IF EXISTS banco_de_dados_desafio_da_construp;

CREATE SCHEMA IF NOT EXISTS banco_de_dados_desafio_da_construp 
DEFAULT CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

USE banco_de_dados_desafio_da_construp;

-- -------------------------------------------------------------------------
-- Tabela marca

DROP TABLE IF EXISTS marca;

CREATE TABLE IF NOT EXISTS marca (
  pk_marca INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(120) NOT NULL,
  PRIMARY KEY (pk_marca),
  UNIQUE INDEX nome_UNICA (nome ASC))
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Tabela material_de_construcao

DROP TABLE IF EXISTS material_de_construcao;

CREATE TABLE IF NOT EXISTS material_de_construcao (
  pk_material_de_construcao INT NOT NULL AUTO_INCREMENT,
  fk_marca INT NOT NULL,
  nome VARCHAR(120) NOT NULL,
  descricao VARCHAR(1000) NOT NULL,
  valor DECIMAL(7,2) NOT NULL,
  momento DATETIME NOT NULL,
  PRIMARY KEY (pk_material_de_construcao),
  INDEX fk_marca (fk_marca ASC),
  CONSTRAINT fk_marca_tabela_material_de_construcao
    FOREIGN KEY (fk_marca)
    REFERENCES marca (pk_marca)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Dados da tabela marca

START TRANSACTION;

USE banco_de_dados_desafio_da_construp;

INSERT INTO marca (pk_marca, nome) VALUES (1, 'Marca A');
INSERT INTO marca (pk_marca, nome) VALUES (2, 'Marca B');
INSERT INTO marca (pk_marca, nome) VALUES (3, 'Marca C');
INSERT INTO marca (pk_marca, nome) VALUES (4, 'Marca D');
INSERT INTO marca (pk_marca, nome) VALUES (5, 'Marca E');
INSERT INTO marca (pk_marca, nome) VALUES (6, 'Marca F');
INSERT INTO marca (pk_marca, nome) VALUES (7, 'Marca G');
INSERT INTO marca (pk_marca, nome) VALUES (8, 'Marca H');
INSERT INTO marca (pk_marca, nome) VALUES (9, 'Marca I');
INSERT INTO marca (pk_marca, nome) VALUES (10, 'Marca J');
INSERT INTO marca (pk_marca, nome) VALUES (11, 'Marca K');
INSERT INTO marca (pk_marca, nome) VALUES (12, 'Marca L');
INSERT INTO marca (pk_marca, nome) VALUES (13, 'Marca M');
INSERT INTO marca (pk_marca, nome) VALUES (14, 'Marca N');
INSERT INTO marca (pk_marca, nome) VALUES (15, 'Marca O');
INSERT INTO marca (pk_marca, nome) VALUES (16, 'Marca P');
INSERT INTO marca (pk_marca, nome) VALUES (17, 'Marca Q');
INSERT INTO marca (pk_marca, nome) VALUES (18, 'Marca R');
INSERT INTO marca (pk_marca, nome) VALUES (19, 'Marca S');
INSERT INTO marca (pk_marca, nome) VALUES (20, 'Marca T');
INSERT INTO marca (pk_marca, nome) VALUES (21, 'Marca U');
INSERT INTO marca (pk_marca, nome) VALUES (22, 'Marca V');
INSERT INTO marca (pk_marca, nome) VALUES (23, 'Marca W');
INSERT INTO marca (pk_marca, nome) VALUES (24, 'Marca X');
INSERT INTO marca (pk_marca, nome) VALUES (25, 'Marca Y');
INSERT INTO marca (pk_marca, nome) VALUES (26, 'Marca Z');

COMMIT;

-- -------------------------------------------------------------------------
-- Dados da tabela material_de_construcao

START TRANSACTION;

USE banco_de_dados_desafio_da_construp;

INSERT INTO material_de_construcao (pk_material_de_construcao, fk_marca, nome, descricao, valor, momento) VALUES
(1, 15, 'Arame', 'Arame liso, 60 metros.', 15.80, '2021-12-17 11:11:06'),
(2, 12, 'Argamassa AC2 15KG', 'Indicada para assentamento em pisos e revestimentos cerâmicos.', 25.20, '2021-12-17 11:12:49'),
(3, 21, 'Areia fina', 'Acabamento interno e reboco. 20Kg.', 7.99, '2021-12-17 11:17:05'),
(4, 3, 'Pedra brita', 'Agregado resultante da fragmentação mecânica de rochas como basalto, granito, gnaisse e calcário.', 8.49, '2021-12-17 11:28:31'),
(5, 6, 'Gesso acartonado', 'Para instalação drywall.', 23.30, '2021-12-17 11:32:47'),
(6, 25, 'Cal da Marca Y', 'Assegura maior agilidade para sua obra.', 13.50, '2021-12-17 11:36:11'),
(7, 17, 'Argila 20KG', 'Leveza, baixa densidade, isolamento térmico e acústico. Indicada para diversas etapas da obra.', 41.90, '2021-12-17 11:41:00'),
(8, 22, 'Caixa d\'água 500L', 'Segurança, resistência e versatilidade.', 600.09, '2021-12-17 11:44:15'),
(9, 8, 'Cisterna 600L', 'Melhor captação de água.', 999.20, '2021-12-17 11:56:55'),
(10, 1, 'Calha de alumínio', 'Bem melhor do que as de aço inoxidável.', 61.30, '2021-12-17 12:01:31'),
(11, 13, 'Cimento comum 25KG', 'Prazo de validade: 3 meses.', 12.11, '2021-12-17 12:17:42'),
(12, 19, 'Tampa de esgoto quadrada pequena', 'Proteja sua casa de escorpiões. 30cm x 30cm.', 81.50, '2021-12-17 12:22:16'),
(13, 24, 'Drywall 1,2x1,8m', 'Resiste ao fogo melhor do que alvenaria convencional. Elevado desempenho acústico.', 39.99, '2021-12-17 12:27:39'),
(14, 9, 'Cortador de piso', 'Melhor custo e benefício, corta sem muitos problemas qualquer tipo de piso. Possui versatilidade e eficiência.', 222.49, '2021-12-17 12:30:40'),
(15, 16, 'Tapume de madeira', 'Comprimento chapa: 2,2m x 1,1m.', 135.55, '2021-12-17 12:35:01'),
(16, 25, 'Viga de Madeira', 'Viga de madeira cambará. É resistente à água e às pragas.', 176.49, '2021-12-17 12:40:09'),
(17, 11, '50 Parafusos', 'Conjunto de 50 parafusos.', 11.60, '2021-12-17 12:47:35'),
(18, 3, 'Tijolos ecológicos', 'Tijolos excelentes no isolamento térmico e acústico.', 528.00, '2021-12-17 12:50:11'),
(19, 2, 'Tijolos adobe', 'Feitos de terra crua, água, palha e fibras naturais, foram moldados manualmente e cozidos ao sol.', 360.00, '2021-12-17 12:55:59'),
(20, 9, '200 Pregos', 'Conjunto de 200 pregos.', 59.99, '2021-12-17 13:04:33'),
(21, 26, 'Areia grossa 20KG', 'Sustentação perfeita para lajes e colunas, útil na produção da argamassas.', 7.80, '2021-12-17 13:08:37'),
(22, 21, '100 Porcas', 'Conjunto de 100 porcas.', 25.90, '2021-12-17 13:12:21'),
(23, 26, 'Cimento Z 25KG', 'Prazo de validade: 3 meses. Possui aditivos. CP II.', 14.29, '2021-12-17 13:15:48'),
(24, 4, 'Cimento D 25KG', 'Prazo de validade: 3 meses. Possui aditivos. CP III.', 13.29, '2021-12-17 13:20:49'),
(25, 10, 'Calha de aço', 'Calha de aço galvanizado. 95x130x3000mm.', 45.70, '2021-12-17 13:29:32'),
(26, 14, 'Tampa de esgoto redonda', 'Diâmetro: 30cm. Material durável.', 77.65, '2021-12-17 13:35:17'),
(27, 17, 'Caixa d\'água 400L', 'Testada e aprovada em todos os cantos do país.', 499.90, '2021-12-17 13:41:06'),
(28, 19, 'Telha ecológica', 'Fabricada a partir de resíduos de fibras vegetais.', 70.00, '2021-12-17 13:47:58'),
(29, 20, 'Telha policarbonato', 'Ondulada. Tem a maior leveza e resistência.', 205.45, '2021-12-17 13:50:01'),
(30, 23, 'Argamassa AC3 20KG', 'Ideal para o assentamento de peças de grandes formatos. Possibilita o piso sobre piso.', 45.00, '2021-12-17 13:58:25'),
(31, 6, 'Argamassa AC1 20KG', 'Melhor argamassa para assentamentos de peças cerâmicas em ambientes internos e térreos.', 14.50, '2021-12-17 14:07:24'),
(32, 14, 'Areia fina N', 'A melhor areia fina para acabamentos. 20Kg.', 6.80, '2021-12-17 14:12:46'),
(33, 9, '100 Pregos', 'Conjunto de 100 pregos.', 37.00, '2021-12-17 14:18:31'),
(34, 17, 'Areia média 20Kg', 'Desde 1988, os melhores produtos para construção.', 4.05, '2021-12-17 14:22:17'),
(35, 25, 'Tijolos cerâmicos 14x19x29', 'Feitos com cerâmica vermelha de alta qualidade e resistência.', 98.90, '2021-12-17 14:27:44'),
(36, 5, 'Tijolos de concreto', 'Super resistentes.', 201.75, '2021-12-17 14:33:16'),
(37, 5, 'Tijolos de concreto celular', 'São mais leves, recomendados para espaços pequenos.', 598.90, '2021-12-17 14:39:26'),
(38, 11, '100 Parafusos', 'Conjunto de 100 parafusos.', 19.99, '2021-12-17 14:44:06'),
(39, 21, '50 Porcas', 'Conjunto de 50 porcas.', 17.50, '2021-12-17 14:48:11'),
(40, 1, 'Tijolos decorativos', 'Possuem um design diferenciado, são perfeitos para paredes internas. Feitos de cerâmica.', 60.00, '2021-12-17 14:52:03'),
(41, 8, 'Bloco Cerâmico 9x19x19cm Avermelhado', 'Ecônomico e ainda capaz de trazer isolamento acústico e térmico para a obra.', 1.29, '2021-12-17 14:59:55'),
(42, 6, 'Argamassa AC3 20KG', 'Possui um maior poder de aderência. É ideal para ser utilizada no assentamento de porcelanatos e revestimentos cerâmicos, bem como para colar grandes placas.', 45.90, '2021-12-17 15:08:01'),
(43, 26, 'Cimento Z 50KG', 'Prazo de validade: 3 meses. Possui aditivos. CP II.', 37.55, '2021-12-17 15:13:33'),
(44, 22, 'Caixa d\'água 250L', 'Segurança, resistência e versatilidade.', 341.50, '2021-12-17 15:17:45'),
(45, 16, 'Arame farpado', 'Arame farpado de torção alternada. 250 metros.', 273.45, '2021-12-17 15:22:20'),
(46, 22, 'Caixa d\'água 700L', 'Segurança, resistência e versatilidade.', 730.50, '2021-12-17 15:27:44'),
(47, 6, 'Sapata de aço', 'Comprimento: 60cm. Dimensão: 60x60cm. Bitola do ferro: 10 mm. Peso: 7,05 Kg.', 36.40, '2021-12-17 15:33:10'),
(48, 24, 'Vergalhões', 'São essenciais para dar suporte às estruturas de concreto.', 33.90, '2021-12-17 15:38:50'),
(49, 13, 'Cimento comum 50KG', 'Prazo de validade: 3 meses.', 27.90, '2021-12-17 15:41:30'),
(50, 20, 'Estribo de Ferro da Marca T', 'Tem a medida certa para a sua necessidade na construção civil.', 1.50, '2021-12-17 15:49:25'),
(51, 3, 'Carrinho de mão 120KG', 'Carrinho de mão de alta resistência.', 389.81, '2021-12-17 15:54:21'),
(52, 7, 'Martelo', 'Ferramenta de percussão constante de uma cabeça de aço temperado e de um cabo de madeira.', 125.50, '2021-12-17 16:09:54'),
(53, 19, '60 Pregos cabeça dupla', 'Use pregos cabeça dupla. Vantagens: Dobro da velocidade na desforma; Redução de custos com mão-de-obra na montagem e desmontagem; Menor desperdício de madeira.', 30.00, '2021-12-17 16:17:38'),
(54, 18, 'Tinta azul', 'Melhor cobertura e melhor acabamento.', 289.90, '2021-12-17 16:23:19'),
(55, 18, 'Tinta vermelha', 'Melhor cobertura e melhor acabamento.', 289.90, '2021-12-17 16:29:59'),
(56, 18, 'Tinta verde', 'Melhor cobertura e melhor acabamento.', 289.90, '2021-12-17 16:35:51'),
(57, 12, 'Argamassa AC2 20KG', 'Indicada para assentamento em pisos e revestimentos cerâmicos.', 26.00, '2021-12-17 16:40:03'),
(58, 18, 'Tinta branca', 'Melhor cobertura e melhor acabamento.', 280.00, '2021-12-17 16:46:36'),
(59, 15, 'Arame', 'Arame liso, 80 metros.', 18.80, '2021-12-17 16:57:00'),
(60, 13, 'Cimento branco 15KG', 'Prazo de validade: 3 meses. Cimento branco.', 55.30, '2021-12-17 17:04:31');

COMMIT;
