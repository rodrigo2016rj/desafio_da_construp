## Sobre
<p>Este sistema foi feito com PHP 8.0.12, Laravel 8.76.2, CSS 3, HTML 5, Javascript e MySQL 5.7.</p>
<p>Este sistema faz:</p>
<ul>
<li>Mostra materiais de construção em uma tabela com as informações: Cadastrado em, nome, descrição, marca, valor e opções.</li>
<li>Filtra a tabela pelo nome do material de construção.</li>
<li>Filtra a tabela pela descrição do material de construção.</li>
<li>Filtra a tabela pelo nome da marca do material de construção.</li>
<li>Filtra a tabela pelo valor do material de construção comparando com um valor mínimo.</li>
<li>Filtra a tabela pelo valor do material de construção comparando com um valor máximo.</li>
<li>Filtra a tabela pelo momento de cadastro do material de construção comparando com uma data mínima.</li>
<li>Filtra a tabela pelo momento de cadastro do material de construção comparando com uma data limite.</li>
<li>Ordena a tabela pelas informações: Nome do material, descrição, nome da marca, valor e momento do cadastro.</li>
<li>Configura a quantidade de registros por página da tabela de materiais de construção.</li>
<li>Aceita diferentes formas de inserir um valor no campo de valor.</li>
<li>Exibe um calendário para facilitar o preenchimento de data nos campos de data.</li>
<li>Permite buscar por combinações de filtros.</li>
<li>Pagina a tabela dando opções de ir para uma página pelo seu número, como também ir para a primeira página, página anterior, página seguinte e última página.</li>
<li>Limpa as informações do formulário de busca.</li>
<li>Cadastra um novo material de construção.</li>
<li>Valida preenchimento obrigatório.</li>
<li>Valida quantidade de caracteres.</li>
<li>Valida se a marca se encontra no banco de dados do sistema.</li>
<li>Valida se o campo foi digitado corretamente.</li>
<li>Valida se o valor não é menor que o mínimo.</li>
<li>Valida se o valor não é maior que o máximo.</li>
<li>Exibe mensagens de validação.</li>
<li>Exibe mensagens de sucesso.</li>
<li>Registra o momento do cadastro de um novo material de construção.</li>
<li>Abrevia a descrição do material de construção na tabela para não causar poluição visual.</li>
<li>Exibe um popup com as informações do material de construção contendo sua descrição completa.</li>
<li>Proteção contra XSS (Cross-site scripting).</li>
<li>Proteção contra SQL injection.</li>
<li>Proteção contra CSRF (Cross-site Request Forgery).</li>
<li>Compatibilidade com diferentes resoluções e tamanhos de telas incluindo mobile.</li>
<li>Edita material de construção.</li>
<li>Deleta material de construção.</li>
</ul>
<br/>

## Motivação
<p>O que me motivou fazer este sistema foi:</p>
<ul>
<li>Tentar ser aprovado em um processo seletivo de emprego.</li>
<li>Demonstrar minhas habilidades de programador.</li>
</ul>
<br/>

## Considerações
<p>Ainda há muito mais que eu posso fazer neste sistema, como por exemplo:</p>
<ul>
<li>Outras formas de ocultar os popups, como um X no canto superior deles por exemplo.</li>
<li>Autocomplete por meio de ajax enquanto o usuário digita o nome da marca tanto para a busca quanto para o cadastro de material de construção.</li>
<li>Máscara nos campos de valor para padronizar o jeito de preencher (exemplo: R$ 10,00).</li>
<li>Login de usuário e página de configurações para escolha de fuso-horário.</li>
<li>Ordenar a tabela ao clicar nas colunas da tabela.</li>
</ul>
<br/>

## Instruções
<p>Utilize o banco de dados contido neste projeto, banco_de_dados_desafio_da_construp.sql.</p>
<p>Utilize para o banco de dados as configurações username root sem senha.</p>
<p>Coloque a pasta desafio_da_construp dentro da pasta htdocs do seu XAMPP.</p>
<p>Na pasta desafio_da_construp utilize o prompt de comando para executar o comando: composer install.</p>
<p>Navegue até a pasta apache\conf\extra do seu XAMPP e no arquivo httpd-vhosts.conf configure um VirtualHost com a porta 80 para este projeto.</p>
<p>Acesse o endereço http://localhost pelo seu navegador.</p>