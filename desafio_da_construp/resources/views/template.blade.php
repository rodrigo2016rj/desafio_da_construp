<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="{{asset('css/template.css')}}" type="text/css" rel="stylesheet"/>
    <script src="{{asset('js/template/interacoes.js')}}"></script>
    @section('head_especifico')
    @show
  </head>
  <body>
    <div id="div_modelo_calendario_template" class="calendario tag_oculta">
      <div id="div_cabecalho_do_calendario_template" class="cabecalho_do_calendario">
        <span class="titulo_do_calendario">Calendário</span>
        <select id="caixa_de_selecao_de_mes_do_calendario_template" 
                class="caixa_de_selecao_de_mes_do_calendario">
          <option class="opcao_mes" value="1">Janeiro</option>
          <option class="opcao_mes" value="2">Fevereiro</option>
          <option class="opcao_mes" value="3">Março</option>
          <option class="opcao_mes" value="4">Abril</option>
          <option class="opcao_mes" value="5">Maio</option>
          <option class="opcao_mes" value="6">Junho</option>
          <option class="opcao_mes" value="7">Julho</option>
          <option class="opcao_mes" value="8">Agosto</option>
          <option class="opcao_mes" value="9">Setembro</option>
          <option class="opcao_mes" value="10">Outubro</option>
          <option class="opcao_mes" value="11">Novembro</option>
          <option class="opcao_mes" value="12">Dezembro</option>
        </select>
        <select id="caixa_de_selecao_de_ano_do_calendario_template" 
                class="caixa_de_selecao_de_ano_do_calendario">
        </select>
      </div>
      <div id="div_corpo_do_calendario_template" class="corpo_do_calendario">
        <div id="div_dias_do_calendario_template" class="dias_do_calendario">
          <div class="celula_do_calendario">
            <span>Dom</span>
          </div>
          <div class="celula_do_calendario">
            <span>Seg</span>
          </div>
          <div class="celula_do_calendario">
            <span>Ter</span>
          </div>
          <div class="celula_do_calendario">
            <span>Qua</span>
          </div>
          <div class="celula_do_calendario">
            <span>Qui</span>
          </div>
          <div class="celula_do_calendario">
            <span>Sex</span>
          </div>
          <div class="celula_do_calendario">
            <span>Sáb</span>
          </div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
          <div class="celula_do_calendario"></div>
        </div>
      </div>
      <div id="div_rodape_do_calendario_template" class="rodape_do_calendario">
        <div id="div_botoes_do_calendario_template" class="botoes_do_calendario">
          <button type="button" id="botao_confirmar_do_calendario_template" 
                  class="botao_do_calendario">Confirmar</button>
        </div>
      </div>
    </div>
    <div id="div_cabecalho_template">
      <h1 id="h1_titulo_do_desafio_template">
        <span id="span_titulo_do_desafio_template">Desafio da ConstrUp</span>
      </h1>
    </div>
    <div id="div_tronco_template">
      @yield('pagina_do_sistema')
    </div>
    <div id="div_rodape_template">
      <div id="div_autor_template">
        <span>Este sistema foi feito por Rodrigo Diniz da Silva.</span>
      </div>
      <div id="div_tecnologias_template">
        <span>Este sistema usa PHP e Laravel.</span>
      </div>
    </div>
  </body>
</html>
