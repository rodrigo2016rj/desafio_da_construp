@extends('template')

@section('head_especifico')
<link href="{{asset('css/materiais_de_construcao.css')}}" type="text/css" rel="stylesheet"/>
<script src="{{asset('js/materiais_de_construcao/controles.js')}}"></script>
<script src="{{asset('js/materiais_de_construcao/ajax.js')}}"></script>
<title>Desafio Construp - Materiais de Construção</title>
@endsection

@section('pagina_do_sistema')
<div id="div_materiais_de_construcao">
  <div id="div_opcoes_da_pagina_mais_opcoes_de_filtro">
    <div id="div_opcoes_da_pagina">
      <div id="div_lista_de_opcoes_da_pagina">
        <a id="link_novo_material_de_construcao" class="opcao_da_pagina">Cadastrar material</a>
      </div>
    </div>
    <form id="form_opcoes_de_filtro" method="get" action="/materiais_de_construcao">
      <h3 id="h3_titulo_filtros">
        <span>Filtros:</span>
      </h3>
      <div id="div_buscar_por_nome">
        <div id="div_label_buscar_por_nome">
          <label id="label_buscar_por_nome" for="campo_buscar_por_nome">
            <span>Nome</span>
          </label>
        </div>
        <div id="div_campo_buscar_por_nome">
          <input type="text" id="campo_buscar_por_nome" name="nome" 
                 value="{{$filtro_nome}}" autocomplete="off"/>
        </div>
      </div>
      <div id="div_buscar_por_descricao">
        <div id="div_label_buscar_por_descricao">
          <label id="label_buscar_por_descricao" for="campo_buscar_por_descricao">
            <span>Descrição</span>
          </label>
        </div>
        <div id="div_campo_buscar_por_descricao">
          <input type="text" id="campo_buscar_por_descricao" name="descricao" 
                 value="{{$filtro_descricao}}" autocomplete="off"/>
        </div>
      </div>
      <div id="div_buscar_por_marca">
        <div id="div_label_buscar_por_marca">
          <label id="label_buscar_por_marca" for="campo_buscar_por_marca">
            <span>Marca</span>
          </label>
        </div>
        <div id="div_campo_buscar_por_marca">
          <input type="text" id="campo_buscar_por_marca" name="marca" 
                 value="{{$filtro_marca}}" autocomplete="off"/>
        </div>
      </div>
      <div id="div_buscar_por_valor_maior_ou_igual">
        <div id="div_label_buscar_por_valor_maior_ou_igual">
          <label id="label_buscar_por_valor_maior_ou_igual" 
                 for="campo_buscar_por_valor_maior_ou_igual">
            <span>Valor, maior ou igual</span>
          </label>
        </div>
        <div id="div_campo_buscar_por_valor_maior_ou_igual">
          <input type="text" id="campo_buscar_por_valor_maior_ou_igual" name="valor_maior_ou_igual" 
                 value="{{$filtro_valor_maior_ou_igual}}" autocomplete="off"/>
        </div>
      </div>
      <div id="div_buscar_por_valor_menor_ou_igual">
        <div id="div_label_buscar_por_valor_menor_ou_igual">
          <label id="label_buscar_por_valor_menor_ou_igual" 
                 for="campo_buscar_por_valor_menor_ou_igual">
            <span>Valor, menor ou igual</span>
          </label>
        </div>
        <div id="div_campo_buscar_por_valor_menor_ou_igual">
          <input type="text" id="campo_buscar_por_valor_menor_ou_igual" name="valor_menor_ou_igual" 
                 value="{{$filtro_valor_menor_ou_igual}}" autocomplete="off"/>
        </div>
      </div>
      <div id="div_buscar_por_cadastrado_a_partir_de">
        <div id="div_label_buscar_por_cadastrado_a_partir_de">
          <label id="label_buscar_por_cadastrado_a_partir_de" 
                 for="campo_buscar_por_cadastrado_a_partir_de">
            <span>Cadastrado a partir de</span>
          </label>
        </div>
        <div id="div_campo_buscar_por_cadastrado_a_partir_de">
          <input type="text" id="campo_buscar_por_cadastrado_a_partir_de" 
                 name="cadastrado_a_partir_de" value="{{$filtro_cadastrado_a_partir_de}}" 
                 autocomplete="off"/>
          <span id="span_calendario_para_o_campo_buscar_por_cadastrado_a_partir_de" 
                class="icone_calendario"></span>
        </div>
      </div>
      <div id="div_buscar_por_cadastrado_antes_de">
        <div id="div_label_buscar_por_cadastrado_antes_de">
          <label id="label_buscar_por_cadastrado_antes_de" 
                 for="campo_buscar_por_cadastrado_antes_de">
            <span>Cadastrado antes de</span>
          </label>
        </div>
        <div id="div_campo_buscar_por_cadastrado_antes_de">
          <input type="text" id="campo_buscar_por_cadastrado_antes_de" name="cadastrado_antes_de" 
                 value="{{$filtro_cadastrado_antes_de}}" autocomplete="off"/>
          <span id="span_calendario_para_o_campo_buscar_por_cadastrado_antes_de" 
                class="icone_calendario"></span>
        </div>
      </div>
      <div id="div_ordenacao">
        <div id="div_label_ordenacao">
          <label id="label_ordenacao" for="caixa_de_selecao_ordenacao">
            <span>Ordenação</span>
          </label>
        </div>
        <div id="div_caixa_de_selecao_ordenacao">
          <span id="span_ordenacao" class="tag_oculta">{{$ordenacao}}</span>
          <select id="caixa_de_selecao_ordenacao" name="ordenacao">
            <option value="padrao" selected="selected">Selecione</option>
            <option value="nome_a_z">Material A..Z</option>
            <option value="nome_z_a">Material Z..A</option>
            <option value="descricao_a_z">Descrição A..Z</option>
            <option value="descricao_z_a">Descrição Z..A</option>
            <option value="marca_a_z">Marca A..Z</option>
            <option value="marca_z_a">Marca Z..A</option>
            <option value="valor_0_9">Valor 0..9</option>
            <option value="valor_9_0">Valor 9..0</option>
            <option value="antigos_primeiros">Antigos primeiros</option>
            <option value="recentes_primeiros">Recentes primeiros</option>
          </select>
        </div>
      </div>
      <div id="div_quantidade_por_pagina">
        <div id="div_label_quantidade_por_pagina">
          <label id="label_quantidade_por_pagina" for="caixa_de_selecao_quantidade_por_pagina">
            <span>Quantidade por página</span>
          </label>
        </div>
        <div id="div_caixa_de_selecao_quantidade_por_pagina">
          <span id="span_quantidade_por_pagina" class="tag_oculta">{{$quantidade_por_pagina}}</span>
          <select id="caixa_de_selecao_quantidade_por_pagina" name="quantidade_por_pagina">
            <option value="padrao" selected="selected">Selecione</option>
            <option value="12">12</option>
            <option value="16">16</option>
            <option value="20">20</option>
            <option value="25">25</option>
            <option value="30">30</option>
            <option value="40">40</option>
            <option value="50">50</option>
            <option value="75">75</option>
            <option value="100">100</option>
          </select>
        </div>
      </div>
      <div id="div_botoes">
        <input type="reset" id="botao_limpar" value="Limpar"/>
        <input type="submit" id="botao_buscar" value="Buscar"/>
      </div>
    </form>
  </div>
  <div id="div_paginacoes_mais_resultado">
    <h2 id="h2_titulo_materiais_de_construcao">
      <span>Materiais de Construção</span>
    </h2>
    <div id="div_mensagem_do_sistema">
      <span id="span_mensagem_do_sistema">{{$mensagem_do_sistema}}</span>
    </div>
    @if ($pagina_atual_dos_materiais_de_construcao)
    <div id="div_paginacao_de_cima">
      @include ('materiais_de_construcao.paginacao_dos_materiais_de_construcao')
    </div>
    @endif
    <div id="div_resultado">
      <table id="tabela_materiais_de_construcao">
        <thead>
          <tr>
            <th class="th_momento">
              <span>Cadastrado em</span>
            </th>
            <th class="th_material">
              <span>Material</span>
            </th>
            <th class="th_marca">
              <span>Marca</span>
            </th>
            <th class="th_valor">
              <span>Valor</span>
            </th>
            <th class="th_opcoes">
              <span>Opções</span>
            </th>
          </tr>
        </thead>
        <tbody>
          @include ('materiais_de_construcao.tabela_de_materiais_de_construcao')
        </tbody>
        <tfoot>
          <tr>
            <th class="th_momento">
              <span>Cadastrado em</span>
            </th>
            <th class="th_material">
              <span>Material</span>
            </th>
            <th class="th_marca">
              <span>Marca</span>
            </th>
            <th class="th_valor">
              <span>Valor</span>
            </th>
            <th class="th_opcoes">
              <span>Opções</span>
            </th>
          </tr>
        </tfoot>
      </table>
    </div>
    @if ($pagina_atual_dos_materiais_de_construcao)
    <div id="div_paginacao_de_baixo">
      @include ('materiais_de_construcao.paginacao_dos_materiais_de_construcao')
    </div>
    @endif
  </div>
</div>
<div id="div_popup_cadastrar" class="tag_oculta">
  @include ('materiais_de_construcao.popup_cadastrar')
</div>
<div id="div_popup_visualizacao" class="tag_oculta"></div>
<div id="div_popup_editar" class="tag_oculta"></div>
<div id="div_popup_deletar" class="tag_oculta"></div>
@endsection
