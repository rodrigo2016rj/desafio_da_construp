<h3 id="h3_titulo_cadastrar_material">
  <span id="span_titulo_cadastrar_material">Cadastrar Material</span>
</h3>
<div id="div_mensagem_de_erro_do_formulario_cadastrar" class="tag_oculta">
  <span id="span_mensagem_de_erro_do_formulario_cadastrar"></span>
</div>
<div id="div_campos_do_formulario_cadastrar">
  <div id="div_inserir_nome">
    <div id="div_label_inserir_nome">
      <label id="label_inserir_nome" for="campo_inserir_nome">
        <span>Nome</span>
      </label>
    </div>
    <div id="div_campo_inserir_nome">
      <input type="text" id="campo_inserir_nome" name="nome" value="" autocomplete="off"/>
    </div>
  </div>
  <div id="div_inserir_descricao">
    <div id="div_label_inserir_descricao">
      <label id="label_inserir_descricao" for="campo_inserir_descricao">
        <span>Descrição</span>
      </label>
    </div>
    <div id="div_campo_inserir_descricao">
      <textarea id="campo_inserir_descricao" name="descricao"></textarea>
    </div>
  </div>
  <div id="div_inserir_marca">
    <div id="div_label_inserir_marca">
      <label id="label_inserir_marca" for="campo_inserir_marca">
        <span>Marca</span>
      </label>
    </div>
    <div id="div_campo_inserir_marca">
      <input type="text" id="campo_inserir_marca" name="marca" value="" autocomplete="off"/>
    </div>
  </div>
  <div id="div_inserir_valor">
    <div id="div_label_inserir_valor">
      <label id="label_inserir_valor" for="campo_inserir_valor">
        <span>Valor</span>
      </label>
    </div>
    <div id="div_campo_inserir_valor">
      <input type="text" id="campo_inserir_valor" name="valor" value="" autocomplete="off"/>
    </div>
  </div>
</div>
<div id="div_botoes_do_formulario_de_cadastro">
  @csrf
  <button id="botao_limpar_do_formulario_de_cadastro">Limpar</button>
  <button id="botao_cadastrar">Cadastrar</button>
</div>
