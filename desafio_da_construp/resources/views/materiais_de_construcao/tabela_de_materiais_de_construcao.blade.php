@foreach ($materiais_de_construcao as $material_de_construcao)
@if ($loop->odd)<tr class="impar">@else<tr class="par">@endif
  <td class="td_momento">
    <span class="span_momento">{{$material_de_construcao['momento']}}</span>
  </td>
  <td class="td_material">
    <div class="div_local_do_nome">
      <a class="link_nome" href="{{$material_de_construcao['id']}}">{{$material_de_construcao['nome']}}</a>
    </div>
    <div class="div_local_da_descricao">
      <span class="span_descricao">{{$material_de_construcao['descricao']}}</span>
    </div>
  </td>
  <td class="td_marca">
    <div class="div_local_da_marca">
      <span class="span_marca">{{$material_de_construcao['nome_da_marca']}}</span>
    </div>
  </td>
  <td class="td_valor">
    <div class="div_local_do_valor">
      <span class="span_valor">R$ {{$material_de_construcao['valor']}}</span>
    </div>
  </td>
  <td class="td_opcoes">
    <div id="div_informacoes_do_material_{{$material_de_construcao['id']}}" class="tag_oculta">
      <div class="div_local_da_visualizacao_do_nome">
        <span>Nome do material:</span>
        <span class="span_visualizacao_do_nome">{{$material_de_construcao['nome']}}</span>
      </div>
      <div class="div_local_da_visualizacao_do_momento">
        <span>Momento do cadastro:</span>
        <span class="span_visualizacao_do_momento">{{$material_de_construcao['momento']}}</span>
      </div>
      <div class="div_local_da_visualizacao_da_marca">
        <span>Marca:</span>
        <span class="span_visualizacao_da_marca">{{$material_de_construcao['nome_da_marca']}}</span>
      </div>
      <div class="div_local_da_visualizacao_da_descricao">
        <span>Descrição:</span>
        <div class="div_visualizacao_da_descricao">{!! $material_de_construcao['descricao_completa'] !!}</div>
      </div>
      <div class="div_local_da_visualizacao_do_valor">
        <span>Valor:</span>
        <span>R$</span>
        <span class="span_visualizacao_do_valor">{{$material_de_construcao['valor']}}</span>
      </div>
    </div>
    <div id="div_formulario_de_edicao_do_material_{{$material_de_construcao['id']}}" 
         class="tag_oculta">
      <h3 class="h3_titulo_editar_material">
        <span class="span_titulo_editar_material">Editar Material</span>
      </h3>
      <div class="div_mensagem_de_erro_do_formulario_editar tag_oculta">
        <span class="span_mensagem_de_erro_do_formulario_editar"></span>
      </div>
      <div class="div_campos_do_formulario_editar">
        <div class="div_editar_nome">
          <div class="div_label_editar_nome">
            <label class="label_editar_nome" for="campo_editar_nome">
              <span>Nome</span>
            </label>
          </div>
          <div class="div_campo_editar_nome">
            <input type="text" class="campo_editar_nome" name="nome" 
                   value="{{$material_de_construcao['nome']}}" autocomplete="off"/>
          </div>
        </div>
        <div class="div_editar_descricao">
          <div class="div_label_editar_descricao">
            <label class="label_editar_descricao" for="campo_editar_descricao">
              <span>Descrição</span>
            </label>
          </div>
          <div class="div_campo_editar_descricao">
            <textarea class="campo_editar_descricao" 
                      name="descricao">{{$material_de_construcao['descricao_pura']}}</textarea>
          </div>
        </div>
        <div class="div_editar_marca">
          <div class="div_label_editar_marca">
            <label class="label_editar_marca" for="campo_editar_marca">
              <span>Marca</span>
            </label>
          </div>
          <div class="div_campo_editar_marca">
            <input type="text" class="campo_editar_marca" name="marca" 
                   value="{{$material_de_construcao['nome_da_marca']}}" autocomplete="off"/>
          </div>
        </div>
        <div class="div_editar_valor">
          <div class="div_label_editar_valor">
            <label class="label_editar_valor" for="campo_editar_valor">
              <span>Valor</span>
            </label>
          </div>
          <div class="div_campo_editar_valor">
            <input type="text" class="campo_editar_valor" name="valor" 
                   value="{{$material_de_construcao['valor']}}" autocomplete="off"/>
          </div>
        </div>
      </div>
      <div class="div_botoes_do_formulario_de_edicao">
        @csrf
        <button class="botao_limpar_do_formulario_de_edicao">Limpar</button>
        <button class="botao_editar">Editar</button>
      </div>
    </div>
    <div id="div_pergunta_sobre_deletar_o_material_{{$material_de_construcao['id']}}" class="tag_oculta">
      <div class="div_pergunta_sobre_deletar_o_material">
        <span>Tem certeza que deseja deletar este material?</span>
      </div>
      <div class="div_botoes_do_formulario_de_delecao">
        @csrf
        <button class="botao_deletar">Sim</button>
        <button class="botao_nao_deletar">Não</button>
      </div>
    </div>
    <div class="div_do_link_visualizar_material_de_construcao">
      <a class="link_visualizar_material_de_construcao" href="{{$material_de_construcao['id']}}">Visualizar</a>
    </div>
    <div class="div_do_link_editar_material_de_construcao">
      <a class="link_editar_material_de_construcao" href="{{$material_de_construcao['id']}}">Editar</a>
    </div>
    <div class="div_do_link_deletar_material_de_construcao">
      <a class="link_deletar_material_de_construcao" href="{{$material_de_construcao['id']}}">Deletar</a>
    </div>
  </td>
</tr>
@endforeach
@if (empty($materiais_de_construcao))
<tr class="impar">
  <td class="coluna_unica" colspan="5">
    <span id="span_mensagem_quando_nao_ha_materiais_de_construcao">Nenhum material de construção foi encontrado, limpe os filtros ou busque por outras informações.</span>
  </td>
</tr>
@endif