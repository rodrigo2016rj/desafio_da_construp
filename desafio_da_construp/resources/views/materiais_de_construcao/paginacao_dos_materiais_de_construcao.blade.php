<a class="primeira_pagina" href="/materiais_de_construcao?pagina=1">Primeira</a>
@if ($pagina_atual_dos_materiais_de_construcao > 1)
  <a class="pagina_anterior" href="/materiais_de_construcao?pagina={{$pagina_atual_dos_materiais_de_construcao - 1}}">Anterior</a>
@else
  <a class="pagina_anterior" href="/materiais_de_construcao?pagina=1">Anterior</a>
@endif
@for ($pagina_dos_materiais_de_construcao=1; $pagina_dos_materiais_de_construcao <= $ultima_pagina_dos_materiais_de_construcao; $pagina_dos_materiais_de_construcao++)
  @if ($pagina_dos_materiais_de_construcao < $pagina_atual_dos_materiais_de_construcao - 3)
    @continue
  @endif
  @if ($pagina_dos_materiais_de_construcao != 1 and $pagina_dos_materiais_de_construcao == $pagina_atual_dos_materiais_de_construcao - 3)
    <span>...</span>
  @endif
  @if ($pagina_dos_materiais_de_construcao > $pagina_atual_dos_materiais_de_construcao + 3)
    <span>...</span>
    @break
  @endif
  @if ($pagina_dos_materiais_de_construcao == $pagina_atual_dos_materiais_de_construcao)
    <a class="pagina_selecionada" href="/materiais_de_construcao?pagina={{$pagina_dos_materiais_de_construcao}}">{{$pagina_dos_materiais_de_construcao}}</a>
  @else
    <a class="pagina" href="/materiais_de_construcao?pagina={{$pagina_dos_materiais_de_construcao}}">{{$pagina_dos_materiais_de_construcao}}</a>
  @endif
@endfor
@if ($pagina_atual_dos_materiais_de_construcao < $ultima_pagina_dos_materiais_de_construcao)
  <a class="pagina_seguinte" href="/materiais_de_construcao?pagina={{$pagina_atual_dos_materiais_de_construcao + 1}}">Seguinte</a>
@else
  <a class="pagina_seguinte" href="/materiais_de_construcao?pagina={{$ultima_pagina_dos_materiais_de_construcao}}">Seguinte</a>
@endif
<a class="ultima_pagina" href="/materiais_de_construcao?pagina={{$ultima_pagina_dos_materiais_de_construcao}}">Última</a>