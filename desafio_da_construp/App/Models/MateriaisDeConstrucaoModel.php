<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\MaterialDeConstrucao;
use App\Models\Entidades\Marca;
use PDO;

final class MateriaisDeConstrucaoModel{

  public function selecionar_materiais_de_construcao($filtros, $ordenacao, $quantidade, $descartar){
    $pdo = DB::connection()->getPdo();

    $where = '';
    $parametros = array();
    foreach($filtros as $chave => $valor){
      $parametro = array();
      switch($chave){
        case 'nome':
          $where .= ' AND material_de_construcao.nome LIKE :nome';
          $parametro['variavel'] = ':nome';
          $parametro['valor'] = "%$valor%";
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'descricao':
          $where .= ' AND material_de_construcao.descricao LIKE :descricao';
          $parametro['variavel'] = ':descricao';
          $parametro['valor'] = "%$valor%";
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'marca':
          $where .= ' AND marca.nome LIKE :marca';
          $parametro['variavel'] = ':marca';
          $parametro['valor'] = "%$valor%";
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'valor_maior_ou_igual':
          $where .= ' AND material_de_construcao.valor >= :valor_maior_ou_igual';
          $parametro['variavel'] = ':valor_maior_ou_igual';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'valor_menor_ou_igual':
          $where .= ' AND material_de_construcao.valor <= :valor_menor_ou_igual';
          $parametro['variavel'] = ':valor_menor_ou_igual';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'cadastrado_a_partir_de':
          $where .= ' AND material_de_construcao.momento >= :cadastrado_a_partir_de';
          $parametro['variavel'] = ':cadastrado_a_partir_de';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'cadastrado_antes_de':
          $where .= ' AND material_de_construcao.momento < :cadastrado_antes_de';
          $parametro['variavel'] = ':cadastrado_antes_de';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
      }
    }
    if(strpos($where, ' AND') === 0){
      $where = substr($where, 4);
      $where = "WHERE$where";
    }

    $order_by = '';
    switch($ordenacao){
      case 'padrao':
        $order_by = 'ORDER BY material_de_construcao.momento DESC, pk_material_de_construcao DESC';
        break;
      case 'nome_a_z':
        $order_by = 'ORDER BY material_de_construcao.nome ASC, pk_material_de_construcao DESC';
        break;
      case 'nome_z_a':
        $order_by = 'ORDER BY material_de_construcao.nome DESC, pk_material_de_construcao DESC';
        break;
      case 'descricao_a_z':
        $order_by = 'ORDER BY material_de_construcao.descricao ASC, pk_material_de_construcao DESC';
        break;
      case 'descricao_z_a':
        $order_by = 'ORDER BY material_de_construcao.descricao DESC, pk_material_de_construcao DESC';
        break;
      case 'marca_a_z':
        $order_by = 'ORDER BY marca.nome ASC, pk_material_de_construcao DESC';
        break;
      case 'marca_z_a':
        $order_by = 'ORDER BY marca.nome DESC, pk_material_de_construcao DESC';
        break;
      case 'valor_0_9':
        $order_by = 'ORDER BY material_de_construcao.valor ASC, pk_material_de_construcao DESC';
        break;
      case 'valor_9_0':
        $order_by = 'ORDER BY material_de_construcao.valor DESC, pk_material_de_construcao DESC';
        break;
      case 'antigos_primeiros':
        $order_by = 'ORDER BY material_de_construcao.momento ASC, pk_material_de_construcao DESC';
        break;
      case 'recentes_primeiros':
        $order_by = 'ORDER BY material_de_construcao.momento DESC, pk_material_de_construcao DESC';
        break;
    }

    $sql = <<<"MySQL"
SELECT pk_material_de_construcao, fk_marca, material_de_construcao.nome, 
material_de_construcao.descricao, marca.nome AS nome_da_marca, material_de_construcao.valor, 
material_de_construcao.momento 
FROM material_de_construcao 
INNER JOIN marca ON fk_marca = pk_marca 
$where
$order_by
LIMIT :descartar, :quantidade
MySQL;

    $pdo_statement = $pdo->prepare($sql);
    foreach($parametros as $parametro){
      $pdo_statement->bindValue($parametro['variavel'], $parametro['valor'], $parametro['tipo']);
    }
    $pdo_statement->bindValue(':descartar', $descartar, PDO::PARAM_INT);
    $pdo_statement->bindValue(':quantidade', $quantidade, PDO::PARAM_INT);

    $pdo_statement->execute();

    $array_resultado = array();
    while($array_valores_do_banco_de_dados = $pdo_statement->fetch(PDO::FETCH_ASSOC)){
      $material_de_construcao = new MaterialDeConstrucao($array_valores_do_banco_de_dados);

      $marca = new Marca();
      $marca->set_pk_marca($array_valores_do_banco_de_dados['fk_marca']);
      $marca->set_nome($array_valores_do_banco_de_dados['nome_da_marca']);
      $material_de_construcao->set_marca($marca);

      $array_resultado[] = $material_de_construcao;
    }

    return $array_resultado;
  }

  public function contar_materiais_de_construcao($filtros){
    $pdo = DB::connection()->getPdo();

    $where = '';
    $parametros = array();
    foreach($filtros as $chave => $valor){
      $parametro = array();
      switch($chave){
        case 'nome':
          $where .= ' AND material_de_construcao.nome LIKE :nome';
          $parametro['variavel'] = ':nome';
          $parametro['valor'] = "%$valor%";
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'descricao':
          $where .= ' AND material_de_construcao.descricao LIKE :descricao';
          $parametro['variavel'] = ':descricao';
          $parametro['valor'] = "%$valor%";
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'marca':
          $where .= ' AND marca.nome LIKE :marca';
          $parametro['variavel'] = ':marca';
          $parametro['valor'] = "%$valor%";
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'valor_maior_ou_igual':
          $where .= ' AND material_de_construcao.valor >= :valor_maior_ou_igual';
          $parametro['variavel'] = ':valor_maior_ou_igual';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'valor_menor_ou_igual':
          $where .= ' AND material_de_construcao.valor <= :valor_menor_ou_igual';
          $parametro['variavel'] = ':valor_menor_ou_igual';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'cadastrado_a_partir_de':
          $where .= ' AND material_de_construcao.momento >= :cadastrado_a_partir_de';
          $parametro['variavel'] = ':cadastrado_a_partir_de';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
        case 'cadastrado_antes_de':
          $where .= ' AND material_de_construcao.momento < :cadastrado_antes_de';
          $parametro['variavel'] = ':cadastrado_antes_de';
          $parametro['valor'] = $valor;
          $parametro['tipo'] = PDO::PARAM_STR;
          $parametros[] = $parametro;
          break;
      }
    }
    if(strpos($where, ' AND') === 0){
      $where = substr($where, 4);
      $where = "WHERE$where";
    }

    $sql = <<<"MySQL"
SELECT COUNT(*) AS quantidade 
FROM material_de_construcao 
INNER JOIN marca ON fk_marca = pk_marca 
$where
MySQL;

    $pdo_statement = $pdo->prepare($sql);
    foreach($parametros as $parametro){
      $pdo_statement->bindValue($parametro['variavel'], $parametro['valor'], $parametro['tipo']);
    }

    $pdo_statement->execute();
    $array_resultado = $pdo_statement->fetch(PDO::FETCH_ASSOC);

    return $array_resultado;
  }

  public function verifica_se_a_marca_existe($nome_da_marca){
    $pdo = DB::connection()->getPdo();

    $sql = <<<'MySQL'
SELECT pk_marca 
FROM marca 
WHERE nome = :nome
MySQL;

    $pdo_statement = $pdo->prepare($sql);
    $pdo_statement->bindValue(':nome', $nome_da_marca, PDO::PARAM_STR);

    $pdo_statement->execute();
    $array_resultado = $pdo_statement->fetchAll(PDO::FETCH_CLASS, Marca::class);

    if(count($array_resultado) == 0){
      $mensagem_do_model = 'A marca digitada não foi encontrada neste sistema.';
      $mensagem_do_model .= ' Por favor, verifique se digitou a marca corretamente.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function cadastrar_material_de_construcao($material_de_construcao){
    $pdo = DB::connection()->getPdo();

    $sql = <<<'MySQL'
INSERT INTO material_de_construcao (fk_marca, nome, descricao, valor, momento) 
VALUES (:fk_marca, :nome, :descricao, :valor, :momento)
MySQL;

    $pdo_statement = $pdo->prepare($sql);
    $pdo_statement->bindValue(':fk_marca', $material_de_construcao->get_fk_marca(), PDO::PARAM_INT);
    $pdo_statement->bindValue(':nome', $material_de_construcao->get_nome(), PDO::PARAM_STR);
    $pdo_statement->bindValue(':descricao', $material_de_construcao->get_descricao(), PDO::PARAM_STR);
    $pdo_statement->bindValue(':valor', $material_de_construcao->get_valor(), PDO::PARAM_STR);
    $pdo_statement->bindValue(':momento', $material_de_construcao->get_momento(), PDO::PARAM_STR);

    $array_resultado = array();
    try{
      $pdo_statement->execute();
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        case 1062:
          $mensagem_do_model = 'Já existe um material de construção cadastrado com uma ou mais';
          $mensagem_do_model .= ' destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem_do_model;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function consultar_material_de_construcao($pk_material_de_construcao){
    $pdo = DB::connection()->getPdo();

    $sql = <<<'MySQL'
SELECT pk_material_de_construcao, fk_marca, material_de_construcao.nome, 
material_de_construcao.descricao, marca.nome AS nome_da_marca, material_de_construcao.valor, 
material_de_construcao.momento 
FROM material_de_construcao 
INNER JOIN marca ON fk_marca = pk_marca 
WHERE pk_material_de_construcao = :pk_material_de_construcao
MySQL;

    $pdo_statement = $pdo->prepare($sql);
    $pdo_statement->bindValue(':pk_material_de_construcao', $pk_material_de_construcao, PDO::PARAM_INT);

    $pdo_statement->execute();
    $array_resultado = $pdo_statement->fetch(PDO::FETCH_ASSOC);

    $array_melhorado = array();
    if(!$array_resultado){
      $mensagem_do_model = "Nenhum material de construção com ID $pk_material_de_construcao foi ";
      $mensagem_do_model .= ' encontrado no banco de dados do sistema.';
      $array_melhorado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $material_de_construcao = new MaterialDeConstrucao($array_resultado);

      $marca = new Marca();
      $marca->set_pk_marca($array_resultado['fk_marca']);
      $marca->set_nome($array_resultado['nome_da_marca']);
      $material_de_construcao->set_marca($marca);

      $array_melhorado[] = $material_de_construcao;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function editar_material_de_construcao($material_de_construcao){
    $pdo = DB::connection()->getPdo();

    $sql = <<<'MySQL'
UPDATE material_de_construcao 
SET nome = :nome, descricao = :descricao, fk_marca = :fk_marca, valor = :valor 
WHERE pk_material_de_construcao = :pk_material_de_construcao
MySQL;

    $pdo_statement = $pdo->prepare($sql);
    $pdo_statement->bindValue(':nome', $material_de_construcao->get_nome(), PDO::PARAM_STR);
    $pdo_statement->bindValue(':descricao', $material_de_construcao->get_descricao(), PDO::PARAM_STR);
    $pdo_statement->bindValue(':fk_marca', $material_de_construcao->get_fk_marca(), PDO::PARAM_INT);
    $pdo_statement->bindValue(':valor', $material_de_construcao->get_valor(), PDO::PARAM_STR);
    $pdo_statement->bindValue(':pk_material_de_construcao', $material_de_construcao->get_pk_material_de_construcao(), PDO::PARAM_INT);

    $array_resultado = array();
    try{
      $pdo_statement->execute();
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        case 1062:
          $mensagem_do_model = 'Já existe um material de construção cadastrado com uma ou mais';
          $mensagem_do_model .= ' destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem_do_model;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function deletar_material_de_construcao($pk_material_de_construcao){
    $pdo = DB::connection()->getPdo();

    $sql = <<<'MySQL'
DELETE 
FROM material_de_construcao 
WHERE pk_material_de_construcao = :pk_material_de_construcao
MySQL;

    $pdo_statement = $pdo->prepare($sql);
    $pdo_statement->bindValue(':pk_material_de_construcao', $pk_material_de_construcao, PDO::PARAM_INT);

    $pdo_statement->execute();
  }

}
