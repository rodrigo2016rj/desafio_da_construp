<?php

namespace App\Models\Entidades;

final class Marca{
  private $pk_marca;
  private $nome;

  public function __construct($array_marca = array()){
    if(isset($array_marca['pk_marca'])){
      $this->pk_marca = $array_marca['pk_marca'];
    }
    if(isset($array_marca['nome'])){
      $this->nome = $array_marca['nome'];
    }
  }

  public function set_pk_marca($pk_marca){
    $this->pk_marca = $pk_marca;
  }

  public function set_nome($nome){
    $this->nome = $nome;
  }

  public function get_pk_marca(){
    return $this->pk_marca;
  }

  public function get_nome(){
    return $this->nome;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 3;
    }
    return -1;
  }

  // O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 100;
    }
    return -1;
  }

}
