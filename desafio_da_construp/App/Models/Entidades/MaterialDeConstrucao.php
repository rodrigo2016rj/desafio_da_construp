<?php

namespace App\Models\Entidades;

final class MaterialDeConstrucao{
  private $pk_material_de_construcao;
  private $fk_marca;
  private $nome;
  private $descricao;
  private $valor;
  private $momento;
  private $marca;

  public function __construct($array_material_de_construcao = array()){
    if(isset($array_material_de_construcao['pk_material_de_construcao'])){
      $this->pk_material_de_construcao = $array_material_de_construcao['pk_material_de_construcao'];
    }
    if(isset($array_material_de_construcao['fk_marca'])){
      $this->fk_marca = $array_material_de_construcao['fk_marca'];
    }
    if(isset($array_material_de_construcao['nome'])){
      $this->nome = $array_material_de_construcao['nome'];
    }
    if(isset($array_material_de_construcao['descricao'])){
      $this->descricao = $array_material_de_construcao['descricao'];
    }
    if(isset($array_material_de_construcao['valor'])){
      $this->valor = $array_material_de_construcao['valor'];
    }
    if(isset($array_material_de_construcao['momento'])){
      $this->momento = $array_material_de_construcao['momento'];
    }
    if(isset($array_material_de_construcao['marca'])){
      $this->marca = $array_material_de_construcao['marca'];
    }
  }

  public function set_pk_material_de_construcao($pk_material_de_construcao){
    $this->pk_material_de_construcao = $pk_material_de_construcao;
  }

  public function set_fk_marca($fk_marca){
    $this->fk_marca = $fk_marca;
  }

  public function set_nome($nome){
    $this->nome = $nome;
  }

  public function set_descricao($descricao){
    $this->descricao = $descricao;
  }

  public function set_valor($valor){
    $this->valor = $valor;
  }

  public function set_momento($momento){
    $this->momento = $momento;
  }

  public function set_marca($marca){
    $this->marca = $marca;
  }

  public function get_pk_material_de_construcao(){
    return $this->pk_material_de_construcao;
  }

  public function get_fk_marca(){
    return $this->fk_marca;
  }

  public function get_nome(){
    return $this->nome;
  }

  public function get_descricao(){
    return $this->descricao;
  }

  public function get_valor(){
    return $this->valor;
  }

  public function get_momento(){
    return $this->momento;
  }

  public function get_marca(){
    return $this->marca;
  }

  public function valor_minimo(){
    return 0.1;
  }

  public function valor_maximo(){
    return 100000.0;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 2;
      case 'descricao':
        return 20;
    }
    return -1;
  }

  // O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 120;
      case 'descricao':
        return 1000;
    }
    return -1;
  }

}
