<?php

namespace App\Http\Controllers;

use App\Models\MateriaisDeConstrucaoModel;
use App\Models\Entidades\MaterialDeConstrucao;
use DateTime;
use DateTimeZone;

final class MateriaisDeConstrucaoController extends TemplateController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 10;

  public function carregar_pagina($redirecionar = false){
    if($redirecionar){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header('Location: /materiais_de_construcao');
      die;
    }

    $valores = $this->get_array_valores();
    $sessao = session();

    /* Variável que guarda a mensagem da página começa inicialmente vazia */
    $valores['mensagem_do_sistema'] = '';

    /* Mostra a mensagem caso exista */
    if($sessao->has('mensagem_do_sistema')){
      $valores['mensagem_do_sistema'] = $sessao->get('mensagem_do_sistema');
      $sessao->forget('mensagem_do_sistema');
      $sessao->save();
    }

    $valores_materiais_de_construcao = $this->mostrar_materiais_de_construcao();
    $valores = array_merge($valores, $valores_materiais_de_construcao);

    return view('materiais_de_construcao/materiais_de_construcao', $valores);
  }

  private function mostrar_materiais_de_construcao(){
    $materiais_de_construcao_model = new MateriaisDeConstrucaoModel();

    $valores_deste_metodo = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $nome = trim($requisicao->get('nome'));
    if($nome !== null and $nome !== ''){
      $filtros['nome'] = $nome;
    }
    $valores_deste_metodo['filtro_nome'] = $nome;

    $descricao = $requisicao->get('descricao');
    if($descricao !== null and $descricao !== ''){
      $filtros['descricao'] = $descricao;
    }
    $valores_deste_metodo['filtro_descricao'] = $descricao;

    $marca = trim($requisicao->get('marca'));
    if($marca !== null and $marca !== ''){
      $filtros['marca'] = $marca;
    }
    $valores_deste_metodo['filtro_marca'] = $marca;

    $valor_maior_ou_igual = trim($requisicao->get('valor_maior_ou_igual'));
    $valor_maior_ou_igual = str_replace(',', '.', $valor_maior_ou_igual);
    $valor_maior_ou_igual = str_replace('R$', '', $valor_maior_ou_igual);
    $valor_maior_ou_igual = trim($valor_maior_ou_igual);
    if($valor_maior_ou_igual !== null){
      $valor_maior_ou_igual = (float) $valor_maior_ou_igual;
      if($valor_maior_ou_igual > 0){
        $filtros['valor_maior_ou_igual'] = $valor_maior_ou_igual;
      }else{
        $valor_maior_ou_igual = '';
      }
    }
    if($valor_maior_ou_igual === ''){
      $valores_deste_metodo['filtro_valor_maior_ou_igual'] = '';
    }else{
      $valor_formatado = number_format((float) $valor_maior_ou_igual, 2, ',', '');
      $valores_deste_metodo['filtro_valor_maior_ou_igual'] = "R$ $valor_formatado";
    }

    $valor_menor_ou_igual = trim($requisicao->get('valor_menor_ou_igual'));
    $valor_menor_ou_igual = str_replace(',', '.', $valor_menor_ou_igual);
    $valor_menor_ou_igual = str_replace('R$', '', $valor_menor_ou_igual);
    $valor_menor_ou_igual = trim($valor_menor_ou_igual);
    if($valor_menor_ou_igual !== null){
      $valor_menor_ou_igual = (float) $valor_menor_ou_igual;
      if($valor_menor_ou_igual > 0){
        $filtros['valor_menor_ou_igual'] = $valor_menor_ou_igual;
      }else{
        $valor_menor_ou_igual = '';
      }
    }
    if($valor_menor_ou_igual === ''){
      $valores_deste_metodo['filtro_valor_menor_ou_igual'] = '';
    }else{
      $valor_formatado = number_format((float) $valor_menor_ou_igual, 2, ',', '');
      $valores_deste_metodo['filtro_valor_menor_ou_igual'] = "R$ $valor_formatado";
    }

    $cadastrado_a_partir_de = trim($requisicao->get('cadastrado_a_partir_de'));
    if($cadastrado_a_partir_de !== null and $cadastrado_a_partir_de !== ''){
      $data_em_que_foi_cadastrado = $this->converter_para_data_do_sql($cadastrado_a_partir_de);

      /* Como a data veio por um formulário preenchido por um usuário e eu armazeno sempre sem 
       * fuso-horário no banco de dados, é necessário desfazer o fuso-horário do usuário antes de 
       * comparar com os registros do banco de dados. Como este sistema não tem usuário logado, 
       * estou usando o horário de Brasília como exemplo. */
      $fuso_horario_do_usuario = new DateTimeZone('-0300'); //Horário de Brasília, o mesmo que 'America/Sao_Paulo'.
      $objeto_date_time = new DateTime($data_em_que_foi_cadastrado, $fuso_horario_do_usuario);

      $sem_fuso_horario = 'GMT';
      $sem_fuso_horario = new DateTimeZone($sem_fuso_horario);

      $objeto_date_time->setTimeZone($sem_fuso_horario);

      $data_em_que_foi_cadastrado = $objeto_date_time->format('Y-m-d H:i:s');
      $filtros['cadastrado_a_partir_de'] = $data_em_que_foi_cadastrado;
    }else{
      $cadastrado_a_partir_de = '';
    }
    $valores_deste_metodo['filtro_cadastrado_a_partir_de'] = $cadastrado_a_partir_de;

    $cadastrado_antes_de = trim($requisicao->get('cadastrado_antes_de'));
    if($cadastrado_antes_de !== null and $cadastrado_antes_de !== ''){
      $data_em_que_foi_cadastrado = $this->converter_para_data_do_sql($cadastrado_antes_de);

      /* Como a data veio por um formulário preenchido por um usuário e eu armazeno sempre sem 
       * fuso-horário no banco de dados, é necessário desfazer o fuso-horário do usuário antes de 
       * comparar com os registros do banco de dados. Como este sistema não tem usuário logado, 
       * estou usando o horário de Brasília como exemplo. */
      $fuso_horario_do_usuario = new DateTimeZone('-0300'); //Horário de Brasília, o mesmo que 'America/Sao_Paulo'.
      $objeto_date_time = new DateTime($data_em_que_foi_cadastrado, $fuso_horario_do_usuario);

      $sem_fuso_horario = 'GMT';
      $sem_fuso_horario = new DateTimeZone($sem_fuso_horario);

      $objeto_date_time->setTimeZone($sem_fuso_horario);

      $data_em_que_foi_cadastrado = $objeto_date_time->format('Y-m-d H:i:s');
      $filtros['cadastrado_antes_de'] = $data_em_que_foi_cadastrado;
    }else{
      $cadastrado_antes_de = '';
    }
    $valores_deste_metodo['filtro_cadastrado_antes_de'] = $cadastrado_antes_de;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'nome_a_z':
      case 'nome_z_a':
      case 'descricao_a_z':
      case 'descricao_z_a':
      case 'marca_a_z':
      case 'marca_z_a':
      case 'valor_0_9':
      case 'valor_9_0':
      case 'antigos_primeiros':
      case 'recentes_primeiros':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores_deste_metodo['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = (int) $requisicao->get('quantidade_por_pagina');
    switch($quantidade_por_pagina){
      case 12:
      case 16:
      case 20:
      case 25:
      case 30:
      case 40:
      case 50:
      case 75:
      case 100:
        $valores_deste_metodo['quantidade_por_pagina'] = $quantidade_por_pagina;
        break;
      default:
        $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;
        $valores_deste_metodo['quantidade_por_pagina'] = 'padrao';
        break;
    }

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_dos_materiais_de_construcao($filtros, $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores_deste_metodo['pagina_atual_dos_materiais_de_construcao'] = $pagina;
    $valores_deste_metodo['ultima_pagina_dos_materiais_de_construcao'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = max($descartar, 0);

    /* Preparando o resultado */
    $materiais_de_construcao = $materiais_de_construcao_model->selecionar_materiais_de_construcao($filtros, $ordenacao, $quantidade_por_pagina, $descartar);
    $array_materiais_de_construcao = array();
    foreach($materiais_de_construcao as $material_de_construcao){
      $array_material_de_construcao = array();

      $array_material_de_construcao['id'] = $material_de_construcao->get_pk_material_de_construcao();

      $momento = $material_de_construcao->get_momento();

      /* Se este projeto tivesse usuário logado, o $momento teria que ser convertido para o 
       * fuso-horário do usuário logado antes de ser apresentado no navegador. 
       * O horário de Brasília está sendo utilizado como um exemplo. */
      $sem_fuso_horario = new DateTimeZone('GMT');
      $objeto_date_time = new DateTime($momento, $sem_fuso_horario);

      $fuso_horario_do_usuario = '-0300'; //Horário de Brasília, o mesmo que 'America/Sao_Paulo'. 
      $fuso_horario_do_usuario = new DateTimeZone($fuso_horario_do_usuario);

      $objeto_date_time->setTimeZone($fuso_horario_do_usuario);

      $momento = $objeto_date_time->format('Y-m-d H:i:s');

      $momento = $this->converter_para_horario_data_do_html($momento);
      $array_material_de_construcao['momento'] = $momento;

      $array_material_de_construcao['nome'] = $material_de_construcao->get_nome();

      $descricao = $material_de_construcao->get_descricao();
      if(mb_strlen($descricao) > 200){
        $descricao = substr($descricao, 0, 200);
        $ultimo_caractere = substr($descricao, -1);
        if($ultimo_caractere === ' ' or $ultimo_caractere === '.'){
          $descricao = substr($descricao, 0, 199);
        }
        $descricao .= '...';
      }
      $array_material_de_construcao['descricao'] = $descricao;

      $array_material_de_construcao['descricao_pura'] = $material_de_construcao->get_descricao();

      $descricao_completa = $material_de_construcao->get_descricao();
      $descricao_completa = htmlspecialchars($descricao_completa);
      $descricao_completa = $this->acrescentar_quebras_de_linha_xhtml($descricao_completa);
      $array_material_de_construcao['descricao_completa'] = $descricao_completa;

      $array_material_de_construcao['id_da_marca'] = $material_de_construcao->get_fk_marca();
      $array_material_de_construcao['nome_da_marca'] = $material_de_construcao->get_marca()->get_nome();

      $valor = $material_de_construcao->get_valor();
      $array_material_de_construcao['valor'] = number_format($valor, 2, ',', '.');

      $array_materiais_de_construcao[] = $array_material_de_construcao;
    }

    $valores_deste_metodo['materiais_de_construcao'] = $array_materiais_de_construcao;

    return $valores_deste_metodo;
  }

  private function calcular_quantidade_de_paginas_dos_materiais_de_construcao($filtros, $quantidade_por_pagina){
    $materiais_de_construcao_model = new MateriaisDeConstrucaoModel();

    $array_resultado = $materiais_de_construcao_model->contar_materiais_de_construcao($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

  public function cadastrar_material_de_construcao_via_ajax(){
    $materiais_de_construcao_model = new MateriaisDeConstrucaoModel();
    $material_de_construcao = new MaterialDeConstrucao();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $nome = trim($requisicao->post('nome'));
    $descricao = trim($requisicao->post('descricao'));
    $marca = trim($requisicao->post('marca'));
    $valor = trim($requisicao->post('valor'));
    $o_usuario_digitou_ponto_no_valor = strpos($valor, '.');
    $valor = str_replace(',', '.', $valor);
    $valor = str_replace('R$', '', $valor);
    $valor = trim($valor);

    /* Validações */
    if(empty($nome)){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' O campo nome do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $minimo = $material_de_construcao->quantidade_minima_de_caracteres('nome');
    $maximo = $material_de_construcao->quantidade_maxima_de_caracteres('nome');
    $quantidade = mb_strlen($nome);
    if($quantidade < $minimo){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= " O campo nome do material precisa ter no mínimo $minimo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= " O campo nome do material não pode ultrapassar $maximo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    if(empty($descricao)){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' O campo descrição do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $minimo = $material_de_construcao->quantidade_minima_de_caracteres('descricao');
    $maximo = $material_de_construcao->quantidade_maxima_de_caracteres('descricao');
    $quantidade = mb_strlen($descricao);
    if($quantidade < $minimo){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= " O campo descrição do material precisa ter no mínimo $minimo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= " O campo descrição do material não pode ultrapassar $maximo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    if(empty($marca)){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' O campo marca do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $array_resultado = $materiais_de_construcao_model->verifica_se_a_marca_existe($marca);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $id_da_marca = $array_resultado[0]->get_pk_marca();

    if($valor === '' or $valor === null){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' O campo valor do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($o_usuario_digitou_ponto_no_valor !== false){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' Não digite ponto no campo valor do material, use a vírgula para separar';
      $mensagem .= ' a parte inteira da parte decimal.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if(!is_numeric($valor) or $valor < 0){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' O campo valor do material precisa ser um número positivo.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $minimo = $material_de_construcao->valor_minimo();
    $maximo = $material_de_construcao->valor_maximo();
    if($valor < $minimo){
      $minimo = number_format($minimo, 2, ',', '');
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= " O campo valor não pode ser menor que $minimo.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($valor > $maximo){
      $maximo = number_format($maximo, 2, ',', '');
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= " O campo valor não pode ser maior que $maximo.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    /* Registrando o momento em que o material será cadastrado. */
    /* Observação: No banco de dados eu armazeno sempre sem fuso horário (timezone). */
    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $momento_atual = $objeto_date_time->format('Y-m-d H:i:s');
    $material_de_construcao->set_momento($momento_atual);

    /* Cadastrando o material no banco de dados */
    $material_de_construcao->set_nome($nome);
    $material_de_construcao->set_descricao($descricao);
    $material_de_construcao->set_fk_marca($id_da_marca);
    $material_de_construcao->set_valor($valor);
    $array_resultado = $materiais_de_construcao_model->cadastrar_material_de_construcao($material_de_construcao);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O material não foi cadastrado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    $sessao = session();
    $mensagem = 'O material foi cadastrado com sucesso.';
    $sessao->put('mensagem_do_sistema', $mensagem);
    $sessao->save();
    $retorno['sucesso'] = $mensagem;
    echo(json_encode($retorno));
    die;
  }

  public function editar_material_de_construcao_via_ajax(){
    $materiais_de_construcao_model = new MateriaisDeConstrucaoModel();
    $material_de_construcao = new MaterialDeConstrucao();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $pk_material_de_construcao = $requisicao->post('id');
    $nome = trim($requisicao->post('nome'));
    $descricao = trim($requisicao->post('descricao'));
    $marca = trim($requisicao->post('marca'));
    $valor = trim($requisicao->post('valor'));
    $o_usuario_digitou_ponto_no_valor = strpos($valor, '.');
    $valor = str_replace(',', '.', $valor);
    $valor = str_replace('R$', '', $valor);
    $valor = trim($valor);

    /* Validações */
    if(!is_numeric($pk_material_de_construcao) or $pk_material_de_construcao <= 0 or floor($pk_material_de_construcao) != $pk_material_de_construcao){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' O ID do material precisa ser um número natural maior que zero.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $array_resultado = $materiais_de_construcao_model->consultar_material_de_construcao($pk_material_de_construcao);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O material não foi editado.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    if(empty($nome)){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' O campo nome do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $minimo = $material_de_construcao->quantidade_minima_de_caracteres('nome');
    $maximo = $material_de_construcao->quantidade_maxima_de_caracteres('nome');
    $quantidade = mb_strlen($nome);
    if($quantidade < $minimo){
      $mensagem = 'O material não foi editado.';
      $mensagem .= " O campo nome do material precisa ter no mínimo $minimo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'O material não foi editado.';
      $mensagem .= " O campo nome do material não pode ultrapassar $maximo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    if(empty($descricao)){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' O campo descrição do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $minimo = $material_de_construcao->quantidade_minima_de_caracteres('descricao');
    $maximo = $material_de_construcao->quantidade_maxima_de_caracteres('descricao');
    $quantidade = mb_strlen($descricao);
    if($quantidade < $minimo){
      $mensagem = 'O material não foi editado.';
      $mensagem .= " O campo descrição do material precisa ter no mínimo $minimo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'O material não foi editado.';
      $mensagem .= " O campo descrição do material não pode ultrapassar $maximo caracteres.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    if(empty($marca)){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' O campo marca do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $array_resultado = $materiais_de_construcao_model->verifica_se_a_marca_existe($marca);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $id_da_marca = $array_resultado[0]->get_pk_marca();

    if($valor === '' or $valor === null){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' O campo valor do material precisa ser preenchido.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($o_usuario_digitou_ponto_no_valor !== false){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' Não digite ponto no campo valor do material, use a vírgula para separar';
      $mensagem .= ' a parte inteira da parte decimal.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if(!is_numeric($valor) or $valor < 0){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' O campo valor do material precisa ser um número positivo.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $minimo = $material_de_construcao->valor_minimo();
    $maximo = $material_de_construcao->valor_maximo();
    if($valor < $minimo){
      $minimo = number_format($minimo, 2, ',', '');
      $mensagem = 'O material não foi editado.';
      $mensagem .= " O campo valor não pode ser menor que $minimo.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    if($valor > $maximo){
      $maximo = number_format($maximo, 2, ',', '');
      $mensagem = 'O material não foi editado.';
      $mensagem .= " O campo valor não pode ser maior que $maximo.";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    /* Atualizando o material no banco de dados */
    $material_de_construcao->set_pk_material_de_construcao($pk_material_de_construcao);
    $material_de_construcao->set_nome($nome);
    $material_de_construcao->set_descricao($descricao);
    $material_de_construcao->set_fk_marca($id_da_marca);
    $material_de_construcao->set_valor($valor);
    $array_resultado = $materiais_de_construcao_model->editar_material_de_construcao($material_de_construcao);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O material não foi editado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    $sessao = session();
    $mensagem = 'O material foi editado com sucesso.';
    $sessao->put('mensagem_do_sistema', $mensagem);
    $sessao->save();
    $retorno['sucesso'] = $mensagem;
    echo(json_encode($retorno));
    die;
  }

  public function deletar_material_de_construcao_via_ajax(){
    $materiais_de_construcao_model = new MateriaisDeConstrucaoModel();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $pk_material_de_construcao = $requisicao->post('id');

    /* Validações */
    if(!is_numeric($pk_material_de_construcao) or $pk_material_de_construcao <= 0 or floor($pk_material_de_construcao) != $pk_material_de_construcao){
      $mensagem = 'O material não foi deletado.';
      $mensagem .= ' O ID do material precisa ser um número natural maior que zero.';
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }
    $array_resultado = $materiais_de_construcao_model->consultar_material_de_construcao($pk_material_de_construcao);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O material não foi deletado.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $retorno['erro'] = $mensagem;
      echo(json_encode($retorno));
      die;
    }

    /* Deletando o material no banco de dados */
    $array_resultado = $materiais_de_construcao_model->deletar_material_de_construcao($pk_material_de_construcao);

    $sessao = session();
    $mensagem = 'O material foi deletado com sucesso.';
    $sessao->put('mensagem_do_sistema', $mensagem);
    $sessao->save();
    $retorno['sucesso'] = $mensagem;
    echo(json_encode($retorno));
    die;
  }

}
