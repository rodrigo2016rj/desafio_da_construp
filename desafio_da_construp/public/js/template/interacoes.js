window.addEventListener("load", function(){
  /* Ajustando altura do tronco para preencher a parte vertical visível da tela: */
  var body = document.getElementsByTagName("body")[0];
  var div_cabecalho_template = document.getElementById("div_cabecalho_template");
  var div_tronco_template = document.getElementById("div_tronco_template");
  var div_rodape_template = document.getElementById("div_rodape_template");
  
  var altura_minima = window.innerHeight;
  
  var estilo_computado = window.getComputedStyle(body);
  altura_minima -= parseInt(estilo_computado.marginTop, 10);
  altura_minima -= parseInt(estilo_computado.borderTopWidth, 10);
  altura_minima -= parseInt(estilo_computado.paddingTop, 10);
  
  estilo_computado = window.getComputedStyle(div_cabecalho_template);
  altura_minima -= parseInt(estilo_computado.marginTop, 10);
  altura_minima -= parseInt(estilo_computado.marginBottom, 10);
  altura_minima -= parseInt(estilo_computado.borderTopWidth, 10);
  altura_minima -= parseInt(estilo_computado.borderBottomWidth, 10);
  altura_minima -= parseInt(estilo_computado.paddingTop, 10);
  altura_minima -= parseInt(estilo_computado.paddingBottom, 10);
  altura_minima -= parseInt(estilo_computado.height, 10);
  
  estilo_computado = window.getComputedStyle(div_tronco_template);
  altura_minima -= parseInt(estilo_computado.marginTop, 10);
  altura_minima -= parseInt(estilo_computado.marginBottom, 10);
  altura_minima -= parseInt(estilo_computado.borderTopWidth, 10);
  altura_minima -= parseInt(estilo_computado.borderBottomWidth, 10);
  altura_minima -= parseInt(estilo_computado.paddingTop, 10);
  altura_minima -= parseInt(estilo_computado.paddingBottom, 10);
  
  estilo_computado = window.getComputedStyle(div_rodape_template);
  altura_minima -= parseInt(estilo_computado.marginTop, 10);
  
  div_tronco_template.style.minHeight = altura_minima + "px";
  
  /* Removendo o foco dos botões quando o cursor sai de cima deles e após o clique */
  var botoes = document.querySelectorAll('input[type="submit"], input[type="reset"], button');
  
  for(var i = 0; i < botoes.length; i++){
    botoes[i].addEventListener("mouseleave", function(){
      this.blur();
    });
    botoes[i].addEventListener("click", function(){
      this.blur();
    });
  }
});
