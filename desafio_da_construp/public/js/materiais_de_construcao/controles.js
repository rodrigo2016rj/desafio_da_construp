window.addEventListener("load", function(){
  var div_popup_cadastrar = document.getElementById("div_popup_cadastrar");
  var div_popup_visualizacao = document.getElementById("div_popup_visualizacao");
  var div_popup_editar = document.getElementById("div_popup_editar");
  var div_popup_deletar = document.getElementById("div_popup_deletar");
  
  var form_opcoes_de_filtro = document.getElementById("form_opcoes_de_filtro");
  var campo_buscar_por_nome = document.getElementById("campo_buscar_por_nome");
  var campo_buscar_por_descricao = document.getElementById("campo_buscar_por_descricao");
  var campo_buscar_por_marca = document.getElementById("campo_buscar_por_marca");
  var campo_buscar_por_valor_maior_ou_igual = document.getElementById("campo_buscar_por_valor_maior_ou_igual");
  var campo_buscar_por_valor_menor_ou_igual = document.getElementById("campo_buscar_por_valor_menor_ou_igual");
  var campo_buscar_por_cadastrado_a_partir_de = document.getElementById("campo_buscar_por_cadastrado_a_partir_de");
  var campo_buscar_por_cadastrado_antes_de = document.getElementById("campo_buscar_por_cadastrado_antes_de");
  var caixa_de_selecao_ordenacao = document.getElementById("caixa_de_selecao_ordenacao");
  var caixa_de_selecao_quantidade_por_pagina = document.getElementById("caixa_de_selecao_quantidade_por_pagina");
  
  /* Colocando o valor da ordenação na caixa de seleção */
  var span_ordenacao = document.getElementById("span_ordenacao");
  caixa_de_selecao_ordenacao.value = span_ordenacao.innerText;
  
  /* Colocando o valor da quantidade por página na caixa de seleção */
  var span_quantidade_por_pagina = document.getElementById("span_quantidade_por_pagina");
  caixa_de_selecao_quantidade_por_pagina.value = span_quantidade_por_pagina.innerText;
  
  /* Guardando backup dos valores iniciais dos filtros */
  var filtro_nome = campo_buscar_por_nome.value;
  var filtro_descricao = campo_buscar_por_descricao.value;
  var filtro_marca = campo_buscar_por_marca.value;
  var filtro_valor_maior_ou_igual = campo_buscar_por_valor_maior_ou_igual.value;
  var filtro_valor_menor_ou_igual = campo_buscar_por_valor_menor_ou_igual.value;
  var filtro_cadastrado_a_partir_de = campo_buscar_por_cadastrado_a_partir_de.value;
  var filtro_cadastrado_antes_de = campo_buscar_por_cadastrado_antes_de.value;
  var ordenacao = caixa_de_selecao_ordenacao.value;
  var quantidade_por_pagina = caixa_de_selecao_quantidade_por_pagina.value;
  
  /* Calendário */
  var span_calendario_para_o_campo_buscar_por_cadastrado_a_partir_de = document.getElementById("span_calendario_para_o_campo_buscar_por_cadastrado_a_partir_de");
  var span_calendario_para_o_campo_buscar_por_cadastrado_antes_de = document.getElementById("span_calendario_para_o_campo_buscar_por_cadastrado_antes_de");
  var div_modelo_calendario_template = document.getElementById("div_modelo_calendario_template");
  var caixa_de_selecao_de_mes_do_calendario_template = document.getElementById("caixa_de_selecao_de_mes_do_calendario_template");
  var caixa_de_selecao_de_ano_do_calendario_template = document.getElementById("caixa_de_selecao_de_ano_do_calendario_template");
  var div_dias_do_calendario_template = document.getElementById("div_dias_do_calendario_template");
  var celulas_do_calendario = document.getElementsByClassName("celula_do_calendario");
  var botao_confirmar_do_calendario_template = document.getElementById("botao_confirmar_do_calendario_template");
  
  var dia_selecionado = null;
  var campo_alvo_do_calendario = null;
  var ocultar_div_modelo_calendario_template = true;
  
  campo_buscar_por_cadastrado_a_partir_de.addEventListener("keyup", function(){
    if(campo_alvo_do_calendario === campo_buscar_por_cadastrado_a_partir_de){
      atualizar_calendario();
    }
  });
  campo_buscar_por_cadastrado_antes_de.addEventListener("keyup", function(){
    if(campo_alvo_do_calendario === campo_buscar_por_cadastrado_antes_de){
      atualizar_calendario();
    }
  });
  
  campo_buscar_por_cadastrado_a_partir_de.addEventListener("click", function(){
    ocultar_div_modelo_calendario_template = false;
  });
  campo_buscar_por_cadastrado_antes_de.addEventListener("click", function(){
    ocultar_div_modelo_calendario_template = false;
  });
  
  span_calendario_para_o_campo_buscar_por_cadastrado_a_partir_de.addEventListener("click", function(){
    ocultar_div_modelo_calendario_template = false;
    mostra_o_calendario(campo_buscar_por_cadastrado_a_partir_de);
  });
  span_calendario_para_o_campo_buscar_por_cadastrado_antes_de.addEventListener("click", function(){
    ocultar_div_modelo_calendario_template = false;
    mostra_o_calendario(campo_buscar_por_cadastrado_antes_de);
  });
  
  function mostra_o_calendario(campo_recebido){
    if(div_modelo_calendario_template.className.indexOf("tag_oculta") >= 0 
       || campo_alvo_do_calendario !== campo_recebido){
      campo_alvo_do_calendario = campo_recebido;
      
      var posicao_x = campo_alvo_do_calendario.getBoundingClientRect().left + window.scrollX;
      var posicao_y = campo_alvo_do_calendario.getBoundingClientRect().top + window.scrollY;
      
      var altura = 0;
      var estilo_computado = window.getComputedStyle(campo_alvo_do_calendario);
      altura += parseInt(estilo_computado.borderTopWidth, 10);
      altura += parseInt(estilo_computado.borderBottomWidth, 10);
      altura += parseInt(estilo_computado.paddingTop, 10);
      altura += parseInt(estilo_computado.paddingBottom, 10);
      altura += parseInt(estilo_computado.height, 10);
      posicao_y += altura;
      
      div_modelo_calendario_template.style.position = "absolute";
      div_modelo_calendario_template.style.top = posicao_y + "px";
      div_modelo_calendario_template.style.left = posicao_x + "px";
      if(window.innerWidth <= 640){
        div_modelo_calendario_template.style.left = "0px";
      }
      
      div_modelo_calendario_template.classList.remove("tag_oculta");
      
      atualizar_calendario();
    }else{
      div_modelo_calendario_template.classList.add("tag_oculta");
    }
  }
  
  function atualizar_calendario(){
    var valor = campo_alvo_do_calendario.value;
    var dia = null;
    var mes = null;
    var ano = null;
    var dias = null;
    var ano_referencia = null;
    
    if(valor !== null && valor.match(/^\d{2}\/\d{2}\/\d{4}$/)){
      dia = valor.substring(0, 2);
      mes = valor.substring(3, 5);
      ano = valor.substring(6, 10);
      
      if(dia.substring(0, 1) === "0"){
        dia = dia.substring(1, 2);
      }
      dia = parseInt(dia, 10);
      
      if(mes.substring(0, 1) === "0"){
        mes = mes.substring(1, 2);
      }
      mes = parseInt(mes, 10);
      
      ano = parseInt(ano, 10);
      
      dias = total_de_dias_do_mes(ano, mes);
      
      ano_referencia = ano;
      
      if(dia > dias){
        dia = dias;
      }
    }else{
      var data_atual = new Date();
      dia = data_atual.getDate();
      mes = data_atual.getMonth() + 1;
      ano = data_atual.getFullYear();
      dias = total_de_dias_do_mes(ano, mes);
      ano_referencia = ano;
    }
    
    dia_selecionado = dia;
    
    caixa_de_selecao_de_mes_do_calendario_template.value = mes;
    
    var menor_ano = ano_referencia - 7;
    var maior_ano = ano_referencia + 4;
    caixa_de_selecao_de_ano_do_calendario_template.innerHTML = "";
    for(var i = menor_ano; i <= maior_ano; i++){
      var elemento_ano = document.createElement("option");
      elemento_ano.setAttribute("value", i);
      elemento_ano.innerText = i;
      caixa_de_selecao_de_ano_do_calendario_template.appendChild(elemento_ano);
    }
    caixa_de_selecao_de_ano_do_calendario_template.value = ano;
    
    gerar_dias_do_mes(dias);
  }
  
  caixa_de_selecao_de_mes_do_calendario_template.addEventListener("change", function(){
    var mes = parseInt(caixa_de_selecao_de_mes_do_calendario_template.value, 10);
    var ano = parseInt(caixa_de_selecao_de_ano_do_calendario_template.value, 10);
    var dias = total_de_dias_do_mes(ano, mes);
    gerar_dias_do_mes(dias);
  });
  
  caixa_de_selecao_de_ano_do_calendario_template.addEventListener("change", function(){
    var mes = parseInt(caixa_de_selecao_de_mes_do_calendario_template.value, 10);
    var ano = parseInt(caixa_de_selecao_de_ano_do_calendario_template.value, 10);
    var dias = total_de_dias_do_mes(ano, mes);
    gerar_dias_do_mes(dias);
  });
  
  function total_de_dias_do_mes(ano, mes){
    var mes_seguinte = mes + 1;
    var total_de_dias_do_mes = new Date(ano, mes_seguinte - 1, 0).getDate();
    return total_de_dias_do_mes;
  }
  
  function gerar_dias_do_mes(dias){
    if(dia_selecionado > dias){
      dia_selecionado = dias;
    }
    
    var mes = parseInt(caixa_de_selecao_de_mes_do_calendario_template.value, 10);
    var ano = parseInt(caixa_de_selecao_de_ano_do_calendario_template.value, 10);
    var dia_da_semana_do_primeiro_dia_do_mes = new Date(ano, mes - 1, 1).getDay() + 1;
    var posicao_inicial = 7; //As posições de 0 a 6 são as legendas.
    var posicao_do_primeiro_dia = dia_da_semana_do_primeiro_dia_do_mes - 1 + posicao_inicial;
    var posicao_do_ultimo_dia = dias - 1 + dia_da_semana_do_primeiro_dia_do_mes - 1 + posicao_inicial;
    var posicao_do_dia_selecionado = dia_selecionado - 1 + dia_da_semana_do_primeiro_dia_do_mes - 1 + posicao_inicial;
    
    var numero_do_dia = 0;
    for(var i = posicao_inicial; i < celulas_do_calendario.length; i++){
      celulas_do_calendario[i].innerHTML = "";
      celulas_do_calendario[i].classList.remove("dia_do_calendario");
      celulas_do_calendario[i].classList.remove("dia_escolhido");
      celulas_do_calendario[i].removeEventListener("click", evento_selecionar_dia);
      
      if(i >= posicao_do_primeiro_dia && i <= posicao_do_ultimo_dia){
        numero_do_dia++;
        celulas_do_calendario[i].classList.remove("tag_oculta");
        celulas_do_calendario[i].classList.add("dia_do_calendario");
        celulas_do_calendario[i].innerHTML = "<span>" + numero_do_dia + "</span>";
        if(i === posicao_do_dia_selecionado){
          celulas_do_calendario[i].classList.add("dia_escolhido");
        }
        celulas_do_calendario[i].addEventListener("click", evento_selecionar_dia);
      }else if(i > posicao_do_ultimo_dia){
        celulas_do_calendario[i].classList.add("tag_oculta");
      }
    }
  }
  
  function evento_selecionar_dia(evento){
    var tag_que_disparou_o_evento = evento.currentTarget;
    for(var i = 0; i < celulas_do_calendario.length; i++){
      celulas_do_calendario[i].classList.remove("dia_escolhido");
    }
    tag_que_disparou_o_evento.classList.add("dia_escolhido");
    dia_selecionado = tag_que_disparou_o_evento.innerText;
  }
  
  botao_confirmar_do_calendario_template.addEventListener("click", function(){
    var dia = dia_selecionado;
    if(dia < 10){
      dia = "0" + dia;
    }
    
    var mes = parseInt(caixa_de_selecao_de_mes_do_calendario_template.value, 10);
    if(mes < 10){
      mes = "0" + mes;
    }
    
    var ano = parseInt(caixa_de_selecao_de_ano_do_calendario_template.value, 10);
    
    var valor = dia + "/" + mes + "/" + ano;
    if(campo_alvo_do_calendario !== null){
      campo_alvo_do_calendario.value = valor;
    }
    
    div_modelo_calendario_template.classList.add("tag_oculta");
  });
  
  /* Comportamento do botão buscar */
  var botao_buscar = document.getElementById("botao_buscar");
  
  botao_buscar.addEventListener("click", function(evento){
    evento.preventDefault();
    
    if(campo_buscar_por_nome.value == ""){
      campo_buscar_por_nome.setAttribute("name", "");
    }
    if(campo_buscar_por_descricao.value == ""){
      campo_buscar_por_descricao.setAttribute("name", "");
    }
    if(campo_buscar_por_marca.value == ""){
      campo_buscar_por_marca.setAttribute("name", "");
    }
    if(campo_buscar_por_valor_maior_ou_igual.value == ""){
      campo_buscar_por_valor_maior_ou_igual.setAttribute("name", "");
    }
    if(campo_buscar_por_valor_menor_ou_igual.value == ""){
      campo_buscar_por_valor_menor_ou_igual.setAttribute("name", "");
    }
    if(campo_buscar_por_cadastrado_a_partir_de.value == ""){
      campo_buscar_por_cadastrado_a_partir_de.setAttribute("name", "");
    }
    if(campo_buscar_por_cadastrado_antes_de.value == ""){
      campo_buscar_por_cadastrado_antes_de.setAttribute("name", "");
    }
    if(caixa_de_selecao_ordenacao.value == "padrao"){
      caixa_de_selecao_ordenacao.setAttribute("name", "");
    }
    if(caixa_de_selecao_quantidade_por_pagina.value == "padrao"){
      caixa_de_selecao_quantidade_por_pagina.setAttribute("name", "");
    }
    
    var elemento_campo_pagina = document.createElement("input");
    elemento_campo_pagina.setAttribute("type", "hidden");
    elemento_campo_pagina.setAttribute("name", "pagina");
    elemento_campo_pagina.setAttribute("value", "1");
    
    form_opcoes_de_filtro.appendChild(elemento_campo_pagina);
    form_opcoes_de_filtro.submit();
  });
  
  /* Comportamento do botão limpar */
  var botao_limpar = document.getElementById("botao_limpar");
  
  botao_limpar.addEventListener("click", function(evento){
    evento.preventDefault();
    
    campo_buscar_por_nome.value = "";
    campo_buscar_por_nome.setAttribute("name", "");
    
    campo_buscar_por_descricao.value = "";
    campo_buscar_por_descricao.setAttribute("name", "");
    
    campo_buscar_por_marca.value = "";
    campo_buscar_por_marca.setAttribute("name", "");
    
    campo_buscar_por_valor_maior_ou_igual.value = "";
    campo_buscar_por_valor_maior_ou_igual.setAttribute("name", "");
    
    campo_buscar_por_valor_menor_ou_igual.value = "";
    campo_buscar_por_valor_menor_ou_igual.setAttribute("name", "");
    
    campo_buscar_por_cadastrado_a_partir_de.value = "";
    campo_buscar_por_cadastrado_a_partir_de.setAttribute("name", "");
    
    campo_buscar_por_cadastrado_antes_de.value = "";
    campo_buscar_por_cadastrado_antes_de.setAttribute("name", "");
    
    caixa_de_selecao_ordenacao.value = "padrao";
    caixa_de_selecao_ordenacao.setAttribute("name", "");
    
    caixa_de_selecao_quantidade_por_pagina.value = "padrao";
    caixa_de_selecao_quantidade_por_pagina.setAttribute("name", "");
    
    var elemento_campo_pagina = document.createElement("input");
    elemento_campo_pagina.setAttribute("type", "hidden");
    elemento_campo_pagina.setAttribute("name", "pagina");
    elemento_campo_pagina.setAttribute("value", "1");
    
    form_opcoes_de_filtro.appendChild(elemento_campo_pagina);
    form_opcoes_de_filtro.submit();
  });
  
  /* Comportamento da mensagem do sistema */
  var span_mensagem_do_sistema = document.getElementById("span_mensagem_do_sistema");
  
  /* Comportamento dos links da paginação */
  var links_da_div_paginacao = document.querySelectorAll("#div_paginacao_de_cima>a, #div_paginacao_de_baixo>a");
  
  for(var i = 0; i < links_da_div_paginacao.length; i++){
    links_da_div_paginacao[i].addEventListener("click", function(evento){
      evento.preventDefault();
      
      /* Os valores dos filtros devem ser aqueles utilizados na busca ao invés do que foi digitado após */
      campo_buscar_por_nome.value = filtro_nome;
      campo_buscar_por_descricao.value = filtro_descricao;
      campo_buscar_por_marca.value = filtro_marca;
      campo_buscar_por_valor_maior_ou_igual.value = filtro_valor_maior_ou_igual;
      campo_buscar_por_valor_menor_ou_igual.value = filtro_valor_menor_ou_igual;
      campo_buscar_por_cadastrado_a_partir_de.value = filtro_cadastrado_a_partir_de;
      campo_buscar_por_cadastrado_antes_de.value = filtro_cadastrado_antes_de;
      caixa_de_selecao_ordenacao.value = ordenacao;
      caixa_de_selecao_quantidade_por_pagina.value = quantidade_por_pagina;
      
      if(campo_buscar_por_nome.value == ""){
        campo_buscar_por_nome.setAttribute("name", "");
      }
      if(campo_buscar_por_descricao.value == ""){
        campo_buscar_por_descricao.setAttribute("name", "");
      }
      if(campo_buscar_por_marca.value == ""){
        campo_buscar_por_marca.setAttribute("name", "");
      }
      if(campo_buscar_por_valor_maior_ou_igual.value == ""){
        campo_buscar_por_valor_maior_ou_igual.setAttribute("name", "");
      }
      if(campo_buscar_por_valor_menor_ou_igual.value == ""){
        campo_buscar_por_valor_menor_ou_igual.setAttribute("name", "");
      }
      if(campo_buscar_por_cadastrado_a_partir_de.value == ""){
        campo_buscar_por_cadastrado_a_partir_de.setAttribute("name", "");
      }
      if(campo_buscar_por_cadastrado_antes_de.value == ""){
        campo_buscar_por_cadastrado_antes_de.setAttribute("name", "");
      }
      if(caixa_de_selecao_ordenacao.value == "padrao"){
        caixa_de_selecao_ordenacao.setAttribute("name", "");
      }
      if(caixa_de_selecao_quantidade_por_pagina.value == "padrao"){
        caixa_de_selecao_quantidade_por_pagina.setAttribute("name", "");
      }
      
      var href = evento.currentTarget.getAttribute("href");
      var pagina = href.replace("/materiais_de_construcao?pagina=", "");
      
      var elemento_campo_pagina = document.createElement("input");
      elemento_campo_pagina.setAttribute("type", "hidden");
      elemento_campo_pagina.setAttribute("name", "pagina");
      elemento_campo_pagina.setAttribute("value", pagina);
      
      form_opcoes_de_filtro.appendChild(elemento_campo_pagina);
      form_opcoes_de_filtro.submit();
    });
  }
  
  /* Comportamento do link_visualizar_material_de_construcao e do link_nome */
  var tabela_materiais_de_construcao = document.getElementById("tabela_materiais_de_construcao");
  var links_que_visualizam_o_material = document.querySelectorAll(".link_nome, .link_visualizar_material_de_construcao");
  var ocultar_div_popup_visualizacao = true;
  
  for(var i = 0; i < links_que_visualizam_o_material.length; i++){
    links_que_visualizam_o_material[i].addEventListener("click", function(evento){
      evento.preventDefault();
      evento.stopPropagation();
      
      div_popup_cadastrar.classList.add("tag_oculta");
      div_popup_editar.classList.add("tag_oculta");
      div_popup_deletar.classList.add("tag_oculta");
      
      span_mensagem_do_sistema.innerText = "";
      
      var id = evento.currentTarget.getAttribute("href");
      if(isNaN(id) || id % 1 != 0 || id <= 0){
        return;
      }
      
      var informacoes_do_material = document.getElementById("div_informacoes_do_material_" + id);
      
      div_popup_visualizacao.innerHTML = informacoes_do_material.innerHTML;
      div_popup_visualizacao.classList.remove("tag_oculta");
      
      var posicao_x = tabela_materiais_de_construcao.getBoundingClientRect().left + window.scrollX;
      
      var estilo_computado = window.getComputedStyle(tabela_materiais_de_construcao);
      posicao_x += parseInt(estilo_computado.borderRightWidth, 10) / 2;
      posicao_x += parseInt(estilo_computado.borderLeftWidth, 10) / 2;
      posicao_x += parseInt(estilo_computado.paddingRight, 10) / 2;
      posicao_x += parseInt(estilo_computado.paddingLeft, 10) / 2;
      posicao_x += parseInt(estilo_computado.width, 10) / 2;
      
      estilo_computado = window.getComputedStyle(div_popup_visualizacao);
      posicao_x -= parseInt(estilo_computado.borderRightWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.borderLeftWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingRight, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingLeft, 10) / 2;
      posicao_x -= parseInt(estilo_computado.width, 10) / 2;
      
      posicao_x = Math.round(posicao_x);
      div_popup_visualizacao.style.left = posicao_x + "px";
      
      var posicao_y = evento.currentTarget.getBoundingClientRect().top + window.scrollY;
      
      posicao_y -= parseInt(estilo_computado.borderTopWidth, 10) / 2;
      posicao_y -= parseInt(estilo_computado.borderBottomWidth, 10) / 2;
      posicao_y -= parseInt(estilo_computado.paddingTop, 10) / 2;
      posicao_y -= parseInt(estilo_computado.paddingBottom, 10) / 2;
      posicao_y -= parseInt(estilo_computado.height, 10) / 2;
      posicao_y = Math.round(posicao_y);
      div_popup_visualizacao.style.top = posicao_y + "px";
      div_popup_visualizacao.tag_clicada = evento.currentTarget;
    });
  }
  
  /* Comportamento do link_novo_material_de_construcao */
  var link_novo_material_de_construcao = document.getElementById("link_novo_material_de_construcao");
  var ocultar_div_popup_cadastrar = true;
  
  link_novo_material_de_construcao.addEventListener("click", function(evento){
    evento.preventDefault();
    evento.stopPropagation();
    
    div_popup_visualizacao.classList.add("tag_oculta");
    div_popup_editar.classList.add("tag_oculta");
    div_popup_deletar.classList.add("tag_oculta");
    
    span_mensagem_do_sistema.innerText = "";
    
    div_popup_cadastrar.classList.remove("tag_oculta");
    
    var posicao_x = tabela_materiais_de_construcao.getBoundingClientRect().left + window.scrollX;
    
    var estilo_computado = window.getComputedStyle(tabela_materiais_de_construcao);
    posicao_x += parseInt(estilo_computado.borderRightWidth, 10) / 2;
    posicao_x += parseInt(estilo_computado.borderLeftWidth, 10) / 2;
    posicao_x += parseInt(estilo_computado.paddingRight, 10) / 2;
    posicao_x += parseInt(estilo_computado.paddingLeft, 10) / 2;
    posicao_x += parseInt(estilo_computado.width, 10) / 2;
    
    estilo_computado = window.getComputedStyle(div_popup_cadastrar);
    posicao_x -= parseInt(estilo_computado.borderRightWidth, 10) / 2;
    posicao_x -= parseInt(estilo_computado.borderLeftWidth, 10) / 2;
    posicao_x -= parseInt(estilo_computado.paddingRight, 10) / 2;
    posicao_x -= parseInt(estilo_computado.paddingLeft, 10) / 2;
    posicao_x -= parseInt(estilo_computado.width, 10) / 2;
    
    posicao_x = Math.round(posicao_x);
    div_popup_cadastrar.style.left = posicao_x + "px";
    
    var posicao_y = link_novo_material_de_construcao.getBoundingClientRect().top + window.scrollY;
    
    div_popup_cadastrar.style.top = posicao_y + "px";
    div_popup_cadastrar.tag_clicada = link_novo_material_de_construcao;
  });
  
  /* Ocultando popups */
  div_modelo_calendario_template.addEventListener("click", function(){
    ocultar_div_modelo_calendario_template = false;
  });
  
  div_popup_visualizacao.addEventListener("click", function(){
    ocultar_div_popup_visualizacao = false;
  });
  
  div_popup_cadastrar.addEventListener("click", function(){
    ocultar_div_popup_cadastrar = false;
  });
  
  var ocultar_div_popup_editar = true;
  div_popup_editar.addEventListener("click", function(){
    ocultar_div_popup_editar = false;
  });
  
  var ocultar_div_popup_deletar = true;
  div_popup_deletar.addEventListener("click", function(){
    ocultar_div_popup_deletar = false;
  });
  
  document.addEventListener("click", function(){
    if(ocultar_div_modelo_calendario_template){
      div_modelo_calendario_template.classList.add("tag_oculta");
    }else{
      ocultar_div_modelo_calendario_template = true;
    }
    if(ocultar_div_popup_visualizacao){
      div_popup_visualizacao.classList.add("tag_oculta");
    }else{
      ocultar_div_popup_visualizacao = true;
    }
    if(ocultar_div_popup_cadastrar){
      div_popup_cadastrar.classList.add("tag_oculta");
    }else{
      ocultar_div_popup_cadastrar = true;
    }
    if(ocultar_div_popup_editar){
      div_popup_editar.classList.add("tag_oculta");
    }else{
      ocultar_div_popup_editar = true;
    }
    if(ocultar_div_popup_deletar){
      div_popup_deletar.classList.add("tag_oculta");
    }else{
      ocultar_div_popup_deletar = true;
    }
  });
  
  /* Comportamento dos popups quando a janela é redimensionada */
  window.addEventListener("resize", function(){
    div_modelo_calendario_template.classList.add("tag_oculta");
    
    var popups_para_reposicionar = Array();
    if(!div_popup_visualizacao.classList.contains("tag_oculta")){
      popups_para_reposicionar.push(div_popup_visualizacao);
    }
    if(!div_popup_cadastrar.classList.contains("tag_oculta")){
      popups_para_reposicionar.push(div_popup_cadastrar);
    }
    if(!div_popup_editar.classList.contains("tag_oculta")){
      popups_para_reposicionar.push(div_popup_editar);
    }
    if(!div_popup_deletar.classList.contains("tag_oculta")){
      popups_para_reposicionar.push(div_popup_deletar);
    }
    
    var posicao_x = tabela_materiais_de_construcao.getBoundingClientRect().left + window.scrollX;
    
    var estilo_computado = window.getComputedStyle(tabela_materiais_de_construcao);
    posicao_x += parseInt(estilo_computado.borderRightWidth, 10) / 2;
    posicao_x += parseInt(estilo_computado.borderLeftWidth, 10) / 2;
    posicao_x += parseInt(estilo_computado.paddingRight, 10) / 2;
    posicao_x += parseInt(estilo_computado.paddingLeft, 10) / 2;
    posicao_x += parseInt(estilo_computado.width, 10) / 2;
    
    for(var i = 0; i < popups_para_reposicionar.length; i++){
      var popup = popups_para_reposicionar[i];
      
      estilo_computado = window.getComputedStyle(popup);
      posicao_x -= parseInt(estilo_computado.borderRightWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.borderLeftWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingRight, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingLeft, 10) / 2;
      posicao_x -= parseInt(estilo_computado.width, 10) / 2;
      posicao_x = Math.round(posicao_x);
      popup.style.left = posicao_x + "px";
      
      var posicao_y = popup.tag_clicada.getBoundingClientRect().top + window.scrollY;
      if(popup.tag_clicada.getAttribute("id") !== "link_novo_material_de_construcao"){
        posicao_y -= parseInt(estilo_computado.borderTopWidth, 10) / 2;
        posicao_y -= parseInt(estilo_computado.borderBottomWidth, 10) / 2;
        posicao_y -= parseInt(estilo_computado.paddingTop, 10) / 2;
        posicao_y -= parseInt(estilo_computado.paddingBottom, 10) / 2;
        posicao_y -= parseInt(estilo_computado.height, 10) / 2;
        posicao_y = Math.round(posicao_y);
      }
      popup.style.top = posicao_y + "px";
    }
  });
  
});
