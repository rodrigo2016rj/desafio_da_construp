window.addEventListener("load", function(){
  var span_mensagem_do_sistema = document.getElementById("span_mensagem_do_sistema");
  var div_popup_cadastrar = document.getElementById("div_popup_cadastrar");
  var div_popup_visualizacao = document.getElementById("div_popup_visualizacao");
  var div_popup_editar = document.getElementById("div_popup_editar");
  var div_popup_deletar = document.getElementById("div_popup_deletar");
  
  /* Comportamento do formulário cadastrar */
  var div_mensagem_de_erro_do_formulario_cadastrar = document.getElementById("div_mensagem_de_erro_do_formulario_cadastrar");
  var span_mensagem_de_erro_do_formulario_cadastrar = document.getElementById("span_mensagem_de_erro_do_formulario_cadastrar");
  var campo_inserir_nome = document.getElementById("campo_inserir_nome");
  var campo_inserir_descricao = document.getElementById("campo_inserir_descricao");
  var campo_inserir_marca = document.getElementById("campo_inserir_marca");
  var campo_inserir_valor = document.getElementById("campo_inserir_valor");
  var div_botoes_do_formulario_de_cadastro = document.getElementById("div_botoes_do_formulario_de_cadastro");
  var botao_limpar_do_formulario_de_cadastro = document.getElementById("botao_limpar_do_formulario_de_cadastro");
  var botao_cadastrar = document.getElementById("botao_cadastrar");
  
  botao_limpar_do_formulario_de_cadastro.addEventListener("click", function(){
    span_mensagem_de_erro_do_formulario_cadastrar.innerText = "";
    div_mensagem_de_erro_do_formulario_cadastrar.classList.add("tag_oculta");
    
    campo_inserir_nome.value = "";
    campo_inserir_descricao.value = "";
    campo_inserir_marca.value = "";
    campo_inserir_valor.value = "";
  });
  
  botao_cadastrar.addEventListener("click", function(){
    var nome = campo_inserir_nome.value;
    var descricao = campo_inserir_descricao.value;
    var marca = campo_inserir_marca.value;
    var valor = campo_inserir_valor.value;
    
    var anti_csrf = div_botoes_do_formulario_de_cadastro.querySelector("input[name='_token']").value;
    
    /* Requisição ajax */
    var conexao_ajax = null;
    if(window.XMLHttpRequest){
      conexao_ajax = new XMLHttpRequest();
    }else if(window.ActiveXObject){
      conexao_ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var tipo = "POST";
    var url_mais = "";
    var url = "materiais_de_construcao/cadastrar_material_de_construcao_via_ajax?" + url_mais;
    var dados_post = {nome: nome, descricao: descricao, marca: marca, valor: valor};
    var resposta = Array();
    conexao_ajax.onreadystatechange = function(){
      if(conexao_ajax.readyState == 4){
        if(conexao_ajax.status == 200){
          resposta = JSON.parse(conexao_ajax.responseText);
          
          if(typeof resposta["erro"] != "undefined"){
            span_mensagem_de_erro_do_formulario_cadastrar.innerText = resposta["erro"];
            div_mensagem_de_erro_do_formulario_cadastrar.classList.remove("tag_oculta");
          }else{
            var pagina_selecionada = document.getElementsByClassName("pagina_selecionada")[0];
            pagina_selecionada.click();
          }
        }
      }
    }
    conexao_ajax.open(tipo, url, true);
    conexao_ajax.setRequestHeader("Content-Type", "application/json");
    conexao_ajax.setRequestHeader("X-CSRF-TOKEN", anti_csrf);
    conexao_ajax.send(JSON.stringify(dados_post));
  });
  
  /* Comportamento do formulário editar e dos links_editar_material_de_construcao */
  var id_do_material_de_construcao_para_editar = null;
  var div_mensagem_de_erro_do_formulario_editar = null;
  var span_mensagem_de_erro_do_formulario_editar = null;
  var campo_editar_nome = null;
  var campo_editar_descricao = null;
  var campo_editar_marca = null;
  var campo_editar_valor = null;
  var div_botoes_do_formulario_de_edicao = null;
  
  var tabela_materiais_de_construcao = document.getElementById("tabela_materiais_de_construcao");
  var links_editar_material_de_construcao = document.getElementsByClassName("link_editar_material_de_construcao");
  
  for(var i = 0; i < links_editar_material_de_construcao.length; i++){
    links_editar_material_de_construcao[i].addEventListener("click", function(evento){
      evento.preventDefault();
      evento.stopPropagation();
      
      div_popup_cadastrar.classList.add("tag_oculta");
      div_popup_visualizacao.classList.add("tag_oculta");
      div_popup_deletar.classList.add("tag_oculta");
      
      span_mensagem_do_sistema.innerText = "";
      
      var id = evento.currentTarget.getAttribute("href");
      if(isNaN(id) || id % 1 != 0 || id <= 0){
        return;
      }
      id_do_material_de_construcao_para_editar = id;
      
      var formulario_de_edicao_do_material = document.getElementById("div_formulario_de_edicao_do_material_" + id);
      
      div_popup_editar.innerHTML = formulario_de_edicao_do_material.innerHTML;
      div_popup_editar.classList.remove("tag_oculta");
      forcar_blur_dos_botoes_da_tag_alvo(div_popup_editar);
      var botao_limpar = div_popup_editar.querySelector(".botao_limpar_do_formulario_de_edicao");
      var botao_editar = div_popup_editar.querySelector(".botao_editar");
      botao_limpar.removeEventListener("click", evento_limpar_do_botao_limpar_do_formulario_de_edicao);
      botao_limpar.addEventListener("click", evento_limpar_do_botao_limpar_do_formulario_de_edicao);
      botao_editar.removeEventListener("click", evento_editar_do_botao_editar);
      botao_editar.addEventListener("click", evento_editar_do_botao_editar);
      
      div_mensagem_de_erro_do_formulario_editar = div_popup_editar.querySelector(".div_mensagem_de_erro_do_formulario_editar");
      span_mensagem_de_erro_do_formulario_editar = div_popup_editar.querySelector(".span_mensagem_de_erro_do_formulario_editar");
      campo_editar_nome = div_popup_editar.querySelector(".campo_editar_nome");
      campo_editar_descricao = div_popup_editar.querySelector(".campo_editar_descricao");
      campo_editar_marca = div_popup_editar.querySelector(".campo_editar_marca");
      campo_editar_valor = div_popup_editar.querySelector(".campo_editar_valor");
      div_botoes_do_formulario_de_edicao = div_popup_editar.querySelector(".div_botoes_do_formulario_de_edicao");
      
      var posicao_x = tabela_materiais_de_construcao.getBoundingClientRect().left + window.scrollX;
      
      var estilo_computado = window.getComputedStyle(tabela_materiais_de_construcao);
      posicao_x += parseInt(estilo_computado.borderRightWidth, 10) / 2;
      posicao_x += parseInt(estilo_computado.borderLeftWidth, 10) / 2;
      posicao_x += parseInt(estilo_computado.paddingRight, 10) / 2;
      posicao_x += parseInt(estilo_computado.paddingLeft, 10) / 2;
      posicao_x += parseInt(estilo_computado.width, 10) / 2;
      
      estilo_computado = window.getComputedStyle(div_popup_editar);
      posicao_x -= parseInt(estilo_computado.borderRightWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.borderLeftWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingRight, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingLeft, 10) / 2;
      posicao_x -= parseInt(estilo_computado.width, 10) / 2;
      
      posicao_x = Math.round(posicao_x);
      div_popup_editar.style.left = posicao_x + "px";
      
      var posicao_y = evento.currentTarget.getBoundingClientRect().top + window.scrollY;
      
      posicao_y -= parseInt(estilo_computado.borderTopWidth, 10) / 2;
      posicao_y -= parseInt(estilo_computado.borderBottomWidth, 10) / 2;
      posicao_y -= parseInt(estilo_computado.paddingTop, 10) / 2;
      posicao_y -= parseInt(estilo_computado.paddingBottom, 10) / 2;
      posicao_y -= parseInt(estilo_computado.height, 10) / 2;
      posicao_y = Math.round(posicao_y);
      div_popup_editar.style.top = posicao_y + "px";
      div_popup_editar.tag_clicada = evento.currentTarget;
    });
  }
  
  function evento_limpar_do_botao_limpar_do_formulario_de_edicao(){
    span_mensagem_de_erro_do_formulario_editar.innerText = "";
    div_mensagem_de_erro_do_formulario_editar.classList.add("tag_oculta");
    
    campo_editar_nome.value = "";
    campo_editar_descricao.value = "";
    campo_editar_marca.value = "";
    campo_editar_valor.value = "";
  }
  
  function evento_editar_do_botao_editar(){
    var id = id_do_material_de_construcao_para_editar;
    var nome = campo_editar_nome.value;
    var descricao = campo_editar_descricao.value;
    var marca = campo_editar_marca.value;
    var valor = campo_editar_valor.value;
    
    var anti_csrf = div_botoes_do_formulario_de_edicao.querySelector("input[name='_token']").value;
    
    /* Requisição ajax */
    var conexao_ajax = null;
    if(window.XMLHttpRequest){
      conexao_ajax = new XMLHttpRequest();
    }else if(window.ActiveXObject){
      conexao_ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var tipo = "POST";
    var url_mais = "";
    var url = "materiais_de_construcao/editar_material_de_construcao_via_ajax?" + url_mais;
    var dados_post = {id: id, nome: nome, descricao: descricao, marca: marca, valor: valor};
    var resposta = Array();
    conexao_ajax.onreadystatechange = function(){
      if(conexao_ajax.readyState == 4){
        if(conexao_ajax.status == 200){
          resposta = JSON.parse(conexao_ajax.responseText);
          
          if(typeof resposta["erro"] != "undefined"){
            span_mensagem_de_erro_do_formulario_editar.innerText = resposta["erro"];
            div_mensagem_de_erro_do_formulario_editar.classList.remove("tag_oculta");
          }else{
            var pagina_selecionada = document.getElementsByClassName("pagina_selecionada")[0];
            pagina_selecionada.click();
          }
        }
      }
    }
    conexao_ajax.open(tipo, url, true);
    conexao_ajax.setRequestHeader("Content-Type", "application/json");
    conexao_ajax.setRequestHeader("X-CSRF-TOKEN", anti_csrf);
    conexao_ajax.send(JSON.stringify(dados_post));
  }
  
  /* Comportamento do formulário deletar e dos links_deletar_material_de_construcao */
  var id_do_material_de_construcao_para_deletar = null;
  var div_botoes_do_formulario_de_delecao = null;
  
  var links_deletar_material_de_construcao = document.getElementsByClassName("link_deletar_material_de_construcao");
  
  for(var i = 0; i < links_deletar_material_de_construcao.length; i++){
    links_deletar_material_de_construcao[i].addEventListener("click", function(evento){
      evento.preventDefault();
      evento.stopPropagation();
      
      div_popup_cadastrar.classList.add("tag_oculta");
      div_popup_visualizacao.classList.add("tag_oculta");
      div_popup_editar.classList.add("tag_oculta");

      span_mensagem_do_sistema.innerText = "";
      
      var id = evento.currentTarget.getAttribute("href");
      if(isNaN(id) || id % 1 != 0 || id <= 0){
        return;
      }
      id_do_material_de_construcao_para_deletar = id;
      
      var informacoes_do_material = document.getElementById("div_informacoes_do_material_" + id);
      var pergunta_sobre_deletar_o_material = document.getElementById("div_pergunta_sobre_deletar_o_material_" + id);
      
      div_popup_deletar.innerHTML = informacoes_do_material.innerHTML;
      div_popup_deletar.innerHTML += pergunta_sobre_deletar_o_material.innerHTML;
      div_popup_deletar.classList.remove("tag_oculta");
      
      forcar_blur_dos_botoes_da_tag_alvo(div_popup_deletar);
      var botao_deletar = div_popup_deletar.querySelector(".botao_deletar");
      var botao_nao_deletar = div_popup_deletar.querySelector(".botao_nao_deletar");
      botao_deletar.removeEventListener("click", evento_do_botao_deletar);
      botao_deletar.addEventListener("click", evento_do_botao_deletar);
      botao_nao_deletar.removeEventListener("click", evento_do_botao_nao_deletar);
      botao_nao_deletar.addEventListener("click", evento_do_botao_nao_deletar);
      
      div_botoes_do_formulario_de_delecao = div_popup_deletar.querySelector(".div_botoes_do_formulario_de_delecao");
      
      var posicao_x = tabela_materiais_de_construcao.getBoundingClientRect().left + window.scrollX;
      
      var estilo_computado = window.getComputedStyle(tabela_materiais_de_construcao);
      posicao_x += parseInt(estilo_computado.borderRightWidth, 10) / 2;
      posicao_x += parseInt(estilo_computado.borderLeftWidth, 10) / 2;
      posicao_x += parseInt(estilo_computado.paddingRight, 10) / 2;
      posicao_x += parseInt(estilo_computado.paddingLeft, 10) / 2;
      posicao_x += parseInt(estilo_computado.width, 10) / 2;
      
      estilo_computado = window.getComputedStyle(div_popup_deletar);
      posicao_x -= parseInt(estilo_computado.borderRightWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.borderLeftWidth, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingRight, 10) / 2;
      posicao_x -= parseInt(estilo_computado.paddingLeft, 10) / 2;
      posicao_x -= parseInt(estilo_computado.width, 10) / 2;
      
      posicao_x = Math.round(posicao_x);
      div_popup_deletar.style.left = posicao_x + "px";
      
      var posicao_y = evento.currentTarget.getBoundingClientRect().top + window.scrollY;
      
      posicao_y -= parseInt(estilo_computado.borderTopWidth, 10) / 2;
      posicao_y -= parseInt(estilo_computado.borderBottomWidth, 10) / 2;
      posicao_y -= parseInt(estilo_computado.paddingTop, 10) / 2;
      posicao_y -= parseInt(estilo_computado.paddingBottom, 10) / 2;
      posicao_y -= parseInt(estilo_computado.height, 10) / 2;
      posicao_y = Math.round(posicao_y);
      div_popup_deletar.style.top = posicao_y + "px";
      div_popup_deletar.tag_clicada = evento.currentTarget;
    });
  }
  
  function evento_do_botao_deletar(){
    var id = id_do_material_de_construcao_para_deletar;
    
    var anti_csrf = div_botoes_do_formulario_de_delecao.querySelector("input[name='_token']").value;
    
    /* Requisição ajax */
    var conexao_ajax = null;
    if(window.XMLHttpRequest){
      conexao_ajax = new XMLHttpRequest();
    }else if(window.ActiveXObject){
      conexao_ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var tipo = "POST";
    var url_mais = "";
    var url = "materiais_de_construcao/deletar_material_de_construcao_via_ajax?" + url_mais;
    var dados_post = {id: id};
    var resposta = Array();
    conexao_ajax.onreadystatechange = function(){
      if(conexao_ajax.readyState == 4){
        if(conexao_ajax.status == 200){
          resposta = JSON.parse(conexao_ajax.responseText);
          
          var pagina_selecionada = document.getElementsByClassName("pagina_selecionada")[0];
          pagina_selecionada.click();
        }
      }
    }
    conexao_ajax.open(tipo, url, true);
    conexao_ajax.setRequestHeader("Content-Type", "application/json");
    conexao_ajax.setRequestHeader("X-CSRF-TOKEN", anti_csrf);
    conexao_ajax.send(JSON.stringify(dados_post));
  }
  
  function evento_do_botao_nao_deletar(evento){
    div_popup_deletar.classList.add("tag_oculta");
  }
  
  function forcar_blur_dos_botoes_da_tag_alvo(tag_alvo){
    /* Removendo o foco dos botões quando o cursor sai de cima deles e após o clique */
    var botoes = tag_alvo.querySelectorAll('input[type="submit"], input[type="reset"], button');
    
    for(var i = 0; i < botoes.length; i++){
      botoes[i].removeEventListener("mouseleave", forcar_blur);
      botoes[i].removeEventListener("click", forcar_blur);
      botoes[i].addEventListener("mouseleave", forcar_blur);
      botoes[i].addEventListener("click", forcar_blur);
    }
  }
  
  function forcar_blur(evento){
    evento.currentTarget.blur();
  }
  
});
