<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Página padrão */
Route::get('/', [App\Http\Controllers\MateriaisDeConstrucaoController::class, 'carregar_pagina']);

/* Materiais de construção */
Route::get('/materiais_de_construcao', [App\Http\Controllers\MateriaisDeConstrucaoController::class, 'carregar_pagina']);
Route::post('/materiais_de_construcao/cadastrar_material_de_construcao_via_ajax', [App\Http\Controllers\MateriaisDeConstrucaoController::class, 'cadastrar_material_de_construcao_via_ajax']);
Route::post('/materiais_de_construcao/editar_material_de_construcao_via_ajax', [App\Http\Controllers\MateriaisDeConstrucaoController::class, 'editar_material_de_construcao_via_ajax']);
Route::post('/materiais_de_construcao/deletar_material_de_construcao_via_ajax', [App\Http\Controllers\MateriaisDeConstrucaoController::class, 'deletar_material_de_construcao_via_ajax']);